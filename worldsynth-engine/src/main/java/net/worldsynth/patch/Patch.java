/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.worldsynth.patch.PatchEvent.PatchEventType;
import net.worldsynth.synth.Synth;

public class Patch {
	
	protected Synth parentSynth;
	
	private ArrayList<PatchListener> listeners = new ArrayList<PatchListener>();
	
	protected ArrayList<ModuleWrapper> moduleWrappers;
	protected ArrayList<ModuleConnector> moduleConnectors;
	
	public Patch(Synth parentSynth) {
		this.parentSynth = parentSynth;
		moduleWrappers = new ArrayList<ModuleWrapper>();
		moduleConnectors = new ArrayList<ModuleConnector>();
	}
	
	public Patch(JsonNode jsonNode, Synth parentSynth) {
		this.parentSynth = parentSynth;
		moduleWrappers = new ArrayList<ModuleWrapper>();
		moduleConnectors = new ArrayList<ModuleConnector>();
		fromJson(jsonNode);
	}
	
	public Synth getParentSynth() {
		return parentSynth;
	}
	
	public String getNewModuleId(ModuleWrapper module) {
		int i = 0;
		String name = module.module.getClass().getSimpleName().toLowerCase();
		
		while (getModuleWrapperByID(name + i) != null) {
			i++;
		}
		return name + i;
	}
	
	public List<ModuleWrapper> getModuleWrapperList() {
		return moduleWrappers;
	}
	
	public void addModuleWrapper(ModuleWrapper wrapper) {
		moduleWrappers.add(wrapper);
		
//		//Register parameters change handler
//		//TODO When implementing caching to a forwards invalidation of other devices
//		mw.module.setOnModuleParametersChange(e -> {
//			WorldSynthEditor.instance.updatePreview();
//			history.addHistoryEvent(new EditorHistoryEventModuleEdit(mw, e.getOldParametersElement(), e.getNewParametersElement()));
//			registerUnsavedChangePerformed();
//		});
//		
//		//Register bypass change handler
//		//TODO When implementing caching to a forwards invalidation of other devices
//		mw.module.setOnModuleBypassChange(e -> {
//			repaint();
//			WorldSynthEditor.instance.updatePreview();
//		});
		
		notifyListenersModuleAdded(wrapper);
	}
	
	public List<ModuleConnector> removeModuleWrapper(ModuleWrapper wrapper) {
		//Get the device connectors currently connected to the device and remove them
		List<ModuleConnector> removableModuleConnectors = getModuleConnectorsByWrapper(wrapper);
		for (ModuleConnector c: removableModuleConnectors) {
			moduleConnectors.remove(c);
		}
		
		//Remove device and notify listeners of it's removal
		moduleWrappers.remove(wrapper);
		notifyListenersModuleRemoved(wrapper);
		
		//Return a list of the device connectors that was removed
		return removableModuleConnectors;
	}
	
	public boolean containsModuleWrapper(ModuleWrapper wrapper) {
		for (ModuleWrapper d: moduleWrappers) {
			if (d == wrapper) {
				return true;
			}
		}
		return false;
	}
	
	public ModuleWrapper getModuleWrapperByID(String wrapperID) {
		for (ModuleWrapper d: moduleWrappers) {
			if (d.wrapperID.equals(wrapperID)) {
				return d;
			}
		}
		return null;
	}
	
	public List<ModuleConnector> getModuleConnectorList() {
		return moduleConnectors;
	}
	
	public void addModuleConnector(ModuleConnector connector) {
		moduleConnectors.add(connector);
	}

	public void removeModuleConnector(ModuleConnector connector) {
		moduleConnectors.remove(connector);
	}
	
	public List<ModuleConnector> getModuleConnectorsByWrapper(ModuleWrapper wrapper) {
		ArrayList<ModuleConnector> connectors = new ArrayList<ModuleConnector>();
		
		for (ModuleConnector c: moduleConnectors) {
			if (c.module2 == wrapper) connectors.add(c);
			else if (c.module1 == wrapper) connectors.add(c);
		}
		
		return connectors;
	}
	
	public List<ModuleConnector> getModuleConnectorsByWrapperIo(ModuleWrapperIO io) {
		ArrayList<ModuleConnector> connectors = new ArrayList<ModuleConnector>();
		
		for (ModuleConnector c: moduleConnectors) {
			if (c.module2Io == io) connectors.add(c);
			else if (c.module1Io == io) connectors.add(c);
		}
		
		return connectors;
	}
	
	public void addPatchListerner(PatchListener listener) {
		listeners.add(listener);
	}
	
	public void removePatchListener(PatchListener listener) {
		listeners.remove(listener);
	}
	
	private void notifyListenersModuleAdded(ModuleWrapper addedWrapper) {
		for (PatchListener listener: listeners) {
			listener.moduleAdded(new PatchEvent(this, PatchEventType.MODULE_ADDED, addedWrapper));
		}
	}
	
	private void notifyListenersModuleRemoved(ModuleWrapper removedWrapper) {
		for (PatchListener listener: listeners) {
			listener.moduleRemoved(new PatchEvent(this, PatchEventType.MODULE_REMOVED, removedWrapper));
		}
	}
	
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode patchNode = objectMapper.createObjectNode();
		
		ArrayNode modulesNode = objectMapper.createArrayNode();
		for (ModuleWrapper mw: moduleWrappers) {
			modulesNode.add(mw.toJson());
		}
		patchNode.set("modules", modulesNode);
		
		ArrayNode connectorsNode = objectMapper.createArrayNode();
		for (ModuleConnector mc: moduleConnectors) {
			connectorsNode.add(mc.toJson());
		}
		patchNode.set("connetctors", connectorsNode);
		
		return patchNode;
	}
	
	protected void fromJson(JsonNode patchNode) {
		JsonNode modulesNode = patchNode.get("modules");
		JsonNode connectorsNode = patchNode.get("connetctors");
		
		//Extract devices and connectors
		if (modulesNode != null) {
			moduleWrappers = extractModuleWrappers(modulesNode);
			if (connectorsNode != null) {
				moduleConnectors = extractModuleConnectors(connectorsNode);
				cleanupModuleConnectors();
			}
		}
	}
	
	protected ArrayList<ModuleWrapper> extractModuleWrappers(JsonNode node) {
		ArrayList<ModuleWrapper> modules = new ArrayList<ModuleWrapper>();
		
		for (JsonNode mn: node) {
			modules.add(new ModuleWrapper(mn, this));
		}
		
		return modules;
	}
	
	protected ArrayList<ModuleConnector> extractModuleConnectors(JsonNode node) {
		ArrayList<ModuleConnector> connectors = new ArrayList<ModuleConnector>();
		
		for (JsonNode cn: node) {
			connectors.add(new ModuleConnector(cn, this));
		}
		
		return connectors;
	}
	
	protected void cleanupModuleConnectors() {
		ArrayList<ModuleConnector> invalidConnectors = new ArrayList<ModuleConnector>();
		
		for (ModuleConnector mc: moduleConnectors) {
			if (!mc.verifyConnection()) {
				invalidConnectors.add(mc);
			}
		}
		
		for (ModuleConnector mc: invalidConnectors) {
			moduleConnectors.remove(mc);
		}
	}
}
