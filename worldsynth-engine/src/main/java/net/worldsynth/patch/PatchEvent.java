/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch;

import java.util.EventObject;

public class PatchEvent extends EventObject {
	private static final long serialVersionUID = -6912881220514049593L;
	
	private PatchEventType eventType;
	private ModuleWrapper moduleWrapper;
	
	public PatchEvent(Object source, PatchEventType eventType, ModuleWrapper moduleWrapper) {
		super(source);
		this.eventType = eventType;
		this.moduleWrapper = moduleWrapper;
	}
	
	public PatchEventType getEventType() {
		return eventType;
	}
	
	public ModuleWrapper getModuleWrapper() {
		return moduleWrapper;
	}
	
	public enum PatchEventType {
		MODULE_ADDED,
		MODULE_REMOVED;
	}
}
