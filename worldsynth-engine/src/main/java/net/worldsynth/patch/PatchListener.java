/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch;

import java.util.EventListener;

public interface PatchListener extends EventListener {
	
	public void moduleAdded(PatchEvent event);
	public void moduleRemoved(PatchEvent event);
	public void moduleModified(PatchEvent event);
}
