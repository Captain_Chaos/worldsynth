/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event.module;

import java.util.EventObject;

public class ModuleIoChangeEvent extends EventObject {
	private static final long serialVersionUID = 3477520059797146016L;

	public ModuleIoChangeEvent(Object source) {
		super(source);
	}
}
