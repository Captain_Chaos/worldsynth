/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event.synth;

import java.util.EventObject;

import net.worldsynth.patch.PatchEvent;

public class SynthEvent extends EventObject {
	private static final long serialVersionUID = -8047917336288454333L;
	
	private SynthEventType eventType;
	private PatchEvent patchEvent;
	
	public SynthEvent(PatchEvent patchEvent, Object source) {
		super(source);
		this.eventType = SynthEventType.PATCH_MODIFIED;
		this.patchEvent = patchEvent;
	}
	
	public SynthEventType getEventType() {
		return eventType;
	}
	
	public PatchEvent getPatchEvent() {
		return patchEvent;
	}
	
	public enum SynthEventType {
		PATCH_MODIFIED;
	}
}
