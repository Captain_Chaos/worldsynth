/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.gen;

import java.util.ArrayList;
import java.util.Random;

public class Permutation {
	
	private final int permutationSize;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	private int[][] dp;
	
	public Permutation(int size, int dims) {
		this(new Random().nextLong(), size, dims);
	}
	
	public Permutation(long seed, int size, int dims) {
		permutationSize = size;
		dp = createPermutatationTable(seed, size, dims);
	}
	
	private int[][] createPermutatationTable(long seed, int size, int dims) {
		int[][] dp = new int[size*2][dims];
		
		for (int i = 0; i < dims; i++) {
			//Create a random generator with supplied seed
			Random r = new Random(seed + i);
			
			//Generate a list containing every integer from 0 inclusive to size exlusive
			ArrayList<Integer> valueTabel = new ArrayList<Integer>();
			for (int j = 0; j < size; j++) {
				valueTabel.add(j);
			}
			
			//create the permutation table
			int[] permutationTable = new int[size];
			
			//Insert the values from the valueTable into the permutation table in a random order
			int pi = 0;
			while (valueTabel.size() > 0) {
				int index = r.nextInt(valueTabel.size());
				permutationTable[pi] = valueTabel.get(index);
				valueTabel.remove(index);
				pi++;
			}
			
			for (int k = 0; k < dp.length; k++) {
				dp[k][i] = permutationTable[k%size];
			}
		}
		
		return dp;
	}
	
	private int localizeGlobal(int val) {
		if (val >= 0) val = val % permutationSize;
		else val = (permutationSize-1)+((val+1)%permutationSize);
		return val;
	}
	
	public int gHash(int dim, int x) {
		return lHash(dim, localizeGlobal(x));
	}
	
	public int gHash(int dim, int x, int y) {
		return lHash(dim, localizeGlobal(x), localizeGlobal(y));
	}
	
	public int gHash(int dim, int x, int y, int z) {
		return lHash(dim, localizeGlobal(x), localizeGlobal(y), localizeGlobal(z));
	}
	
	public double gUnitHash(int dim, int x) {
		return lUnitHash(dim, localizeGlobal(x));
	}
	
	public double gUnitHash(int dim, int x, int y) {
		return lUnitHash(dim, localizeGlobal(x), localizeGlobal(y));
	}
	
	public double gUnitHash(int dim, int x, int y, int z) {
		return lUnitHash(dim, localizeGlobal(x), localizeGlobal(y), localizeGlobal(z));
	}
	
	public int lHash(int dim, int x) {
		return dp[x][dim];
	}
	
	public int lHash(int dim, int x, int y) {
		return dp[dp[x][dim]+y][dim];
	}
	
	public int lHash(int dim, int x, int y, int z) {
		return dp[dp[dp[x][dim]+y][dim]+z][dim];
	}
	
	public double lUnitHash(int dim, int x) {
		return (double) lHash(dim, x) / (double) permutationSize;
	}
	
	public double lUnitHash(int dim, int x, int y) {
		return (double) lHash(dim, x, y) / (double) permutationSize;
	}
	
	public double lUnitHash(int dim, int x, int y, int z) {
		return (double) lHash(dim, x, y, z) / (double) permutationSize;
	}
}
