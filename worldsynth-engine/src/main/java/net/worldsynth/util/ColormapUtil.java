/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util;

import net.worldsynth.util.math.MathHelperScalar;

public class ColormapUtil {
	
	/**
	 * Applies a mask to the given colormap. Areas that are masked out are replaced
	 * with black through linear interpolation.
	 * 
	 * @param map              The map to be masked
	 * @param mask
	 * @param normalizedHeight
	 */
	public static void applyMask(final float[][][] map, float[][] mask, float normalizedHeight) {
		int mpw = map.length;
		int mpl = map[0].length;
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float m = mask[u][v]/normalizedHeight;
				for (int i = 0; i < 3; i++) {
					map[u][v][i] = MathHelperScalar.clamp(map[u][v][i] * m, 0.0f, 1.0f);
				}
			}
		}
	}
	
	/**
	 * Applies a mask to the given colormap. Areas that are masked out are replaced
	 * with the input colormap through linear interpolation.
	 * 
	 * @param map              The map to be masked
	 * @param inputMap         The input map to yes where the map is masked out
	 * @param mask
	 * @param normalizedHeight
	 */
	public static void applyMask(final float[][][] map, float[][][] inputMap, float[][] mask, float normalizedHeight) {
		int mpw = map.length;
		int mpl = map[0].length;
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float m = mask[u][v]/normalizedHeight;
				for (int i = 0; i < 3; i++) {
					map[u][v][i] = MathHelperScalar.clamp((map[u][v][i] - inputMap[u][v][i]) * m + inputMap[u][v][i], 0.0f, 1.0f);
				}
			}
		}
	}
}
