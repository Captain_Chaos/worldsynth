/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import java.util.HashSet;
import java.util.Set;

import javafx.scene.paint.Color;
import net.worldsynth.biome.Biome;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

public class DatatypeMaterialmap extends AbstractDatatype implements IMaterialmap {
	
	private MaterialState<?, ?>[][] materialmap;
	
	/**
	 * The extent of the data
	 */
	public final BuildExtent extent;
	
	/**
	 * The resolutions of units per materialmap point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the materialmap
	 */
	public final int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeMaterialmap() {
		this(new BuildExtent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the materialmap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeMaterialmap(BuildExtent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		mapPointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public void setMaterialmap(MaterialState<?, ?>[][] materialmap) {
		int paramWidth = materialmap.length;
		int paramLength = materialmap[0].length;
		if (paramWidth != mapPointsWidth || paramLength != mapPointsLength) {
			throw new IllegalArgumentException("Heightmap data has wrong size. Expected a " + mapPointsWidth + "x" + mapPointsLength + " map, got a " + paramWidth + "x" + paramLength  + " map.");
		}
		
		//Replace all null entries with default air
		MaterialState<?, ?> air = MaterialRegistry.getDefaultAir().getDefaultState();
		for (int u = 0; u < mapPointsWidth; u++) {
			for (int v = 0; v < mapPointsLength; v++) {
				if (materialmap[u][v] == null) materialmap[u][v] = air;
			}
		}
		
		this.materialmap = materialmap;
	}
	
	@Override
	public MaterialState<?, ?>[][] getMaterialmap() {
		return materialmap;
	}
	
	@Override
	public boolean isLocalContained(int x, int z) {
		if (x < 0 || x >= mapPointsWidth || z < 0 || z >= mapPointsLength) {
			return false;
		}
		return true;
	}
	
	@Override
	public MaterialState<?, ?> getLocalMaterial(int x, int z) {
		return materialmap[x][z];
	}
	
	@Override
	public boolean isGlobalContained(double x, double z) {
		return extent.isContained(x, z);
	}
	
	@Override
	public MaterialState<?, ?> getGlobalMaterial(double x, double z) {
		x = (x - extent.getX()) / resolution;
		z = (z - extent.getZ()) / resolution;
		
		return materialmap[(int) x][(int) z];
	}
	
	public Set<MaterialState<?, ?>> getMaterialset() {
		HashSet<MaterialState<?, ?>> materialset = new HashSet<MaterialState<?, ?>>();

		for (int u = 0; u < mapPointsWidth; u++) {
			for (int v = 0; v < mapPointsLength; v++) {
				materialset.add(materialmap[u][v]);
			}
		}
		
		return materialset;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 170, 0);
	}

	@Override
	public String getDatatypeName() {
		return "Materialmap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeMaterialmap dmm = new DatatypeMaterialmap(extent, resolution);
		if (materialmap != null) {
			MaterialState<?, ?>[][] newMap = new MaterialState[mapPointsWidth][mapPointsLength];
			for (int u = 0; u < mapPointsWidth; u++) {
				newMap[u] = materialmap[u].clone();
			}
			dmm.materialmap = newMap;
		}
		return dmm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeMaterialmap(new BuildExtent(x, y, z, width, height, length), resolution);
	}
}
