/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.extent.BuildExtent;

public class DatatypeVectormap extends AbstractDatatype implements IVectormap {
	
	private float[][][] vectormap;
	
	/**
	 * The extent of the data
	 */
	public final BuildExtent extent;
	
	/**
	 * The resolutions of units per vectorfield point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the vectorfield
	 */
	public final int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeVectormap() {
		this(new BuildExtent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the vectorfield. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeVectormap(BuildExtent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		mapPointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public void setVectormap(float[][][] vctormap) {
		int paramWidth = vctormap.length;
		int paramLength = vctormap[0].length;
		if (paramWidth != mapPointsWidth || paramLength != mapPointsLength) {
			throw new IllegalArgumentException("Vectormap data has wrong size. Expected a " + mapPointsWidth + "x" + mapPointsLength + " map, got a " + paramWidth + "x" + paramLength  + " map.");
		}
		this.vectormap = vctormap;
	}
	
	@Override
	public float[][][] getVectormap() {
		return vectormap;
	}
	
	@Override
	public boolean isLocalContained(int x, int z) {
		if (x < 0 || x >= mapPointsWidth || z < 0 || z >= mapPointsLength) {
			return false;
		}
		return true;
	}
	
	@Override
	public float[] getLocalVector(int x, int z) {
		return vectormap[x][z];
	}
	
	@Override
	public float[] getLocalLerpVector(double x, double z) {
		int x1 = (int) x;
		int x2 = x1+1;
		x2 = Math.min(x2, mapPointsWidth-1);
		int z1 = (int) z;
		int z2 = z1+1;
		z2 = Math.min(z2, mapPointsLength-1);
		
		double u = x - Math.floor(x);
		double v = z - Math.floor(z);
		
		float xVal = (float) ((1.0-v)*(1.0-u)*vectormap[x1][z1][0] + (1.0-v)*u*vectormap[x2][z1][0] + v*(1.0-u)*vectormap[x1][z2][0] + v*u*vectormap[x2][z2][0]);
		float zVal = (float) ((1.0-v)*(1.0-u)*vectormap[x1][z1][1] + (1.0-v)*u*vectormap[x2][z1][1] + v*(1.0-u)*vectormap[x1][z2][1] + v*u*vectormap[x2][z2][1]);
		
		return new float[]{xVal, zVal};
	}
	
	@Override
	public boolean isGlobalContained(double x, double z) {
		return extent.isContained(x, z);
	}
	
	@Override
	public float[] getGlobalVector(double x, double z) {
		x = (x - extent.getX()) / resolution;
		z = (z - extent.getZ()) / resolution;
		
		return vectormap[(int) x][(int) z];
	}
	
	@Override
	public float[] getGlobalLerpVector(double x, double z) {
		double localX = (x - extent.getX()) / resolution;
		double localZ = (z - extent.getZ()) / resolution;
		
		return getLocalLerpVector(localX, localZ);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(146, 0, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Vectormap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeVectormap dhm = new DatatypeVectormap(extent, resolution);
		if (vectormap != null) {
			float[][][] newMap = new float[mapPointsWidth][mapPointsLength][];
			for (int u = 0; u < mapPointsWidth; u++) {
				for (int v = 0; v < mapPointsLength; v++) {
					newMap[u][v] = vectormap[u][v].clone();
				}
			}
			dhm.vectormap = newMap;
		}
		return dhm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeVectormap(new BuildExtent(x, y, z, width, height, length), resolution);
	}
}
