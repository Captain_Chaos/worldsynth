/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

public interface IValuespace {
	
	public void setValuespace(float[][][] valuespace);
	
	public float[][][] getValuespace();
	
	public boolean isLocalContained(int x, int y, int z);
	
	public float getLocalValue(int x, int y, int z);	
	
	public float getLocalLerpValue(double x, double y, double z);

	public boolean isGlobalContained(double x, double y, double z);
	
	public float getGlobalValue(double x, double y, double z);
	
	public float getGlobalLerpValue(double x, double y, double z);
	
}
