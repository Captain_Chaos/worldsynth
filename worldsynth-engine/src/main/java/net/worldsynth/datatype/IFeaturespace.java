/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import net.worldsynth.featurepoint.Featurepoint3D;

public interface IFeaturespace {
	
	public void setFeaturepoints(Featurepoint3D[] points);
	
	public Featurepoint3D[] getFeaturepoints();

	public boolean isGlobalContained(double x, double y, double z);
}
