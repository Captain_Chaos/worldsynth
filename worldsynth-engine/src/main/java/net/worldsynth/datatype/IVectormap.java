/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

public interface IVectormap {
	
	public void setVectormap(float[][][] vectormap);
	
	public float[][][] getVectormap();
	
	public boolean isLocalContained(int x, int z);
	
	public float[] getLocalVector(int x, int z);	
	
	public float[] getLocalLerpVector(double x, double z);

	public boolean isGlobalContained(double x, double z);
	
	public float[] getGlobalVector(double x, double z);
	
	public float[] getGlobalLerpVector(double x, double z);
	
}
