/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.extent.BuildExtent;

public class DatatypeColormap extends AbstractDatatype implements IColormap {
	
	private float[][][] colormap;
	
	/**
	 * The extent of the data
	 */
	public final BuildExtent extent;
	
	/**
	 * The resolutions of units per colormap point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the colormap
	 */
	public final int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeColormap() {
		this(new BuildExtent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the colormap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeColormap(BuildExtent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		mapPointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public void setColormap(float[][][] colormap) {
		int paramWidth = colormap.length;
		int paramLength = colormap[0].length;
		if (paramWidth != mapPointsWidth || paramLength != mapPointsLength) {
			throw new IllegalArgumentException("Colormap data has wrong size. Expected a " + mapPointsWidth + "x" + mapPointsLength + " map, got a " + paramWidth + "x" + paramLength  + " map.");
		}
		this.colormap = colormap;
	}
	
	@Override
	public float[][][] getColormap() {
		return colormap;
	}
	
	@Override
	public boolean isLocalContained(int x, int z) {
		if (x < 0 || x >= mapPointsWidth || z < 0 || z >= mapPointsLength) {
			return false;
		}
		return true;
	}
	
	@Override
	public float[] getLocalColor(int x, int z) {
		return colormap[x][z];
	}
	
	@Override
	public float[] getLocalLerpColor(double x, double z) {
		int x1 = (int) x;
		int x2 = x1+1;
		x2 = Math.min(x2, mapPointsWidth-1);
		int z1 = (int) z;
		int z2 = z1+1;
		z2 = Math.min(z2, mapPointsLength-1);
		
		double u = x - Math.floor(x);
		double v = z - Math.floor(z);
		
		float[] val = new float[colormap[x1][z1].length];
		for (int i = 0; i < val.length; i++) {
			val[i] = (float) ((1.0-v)*(1.0-u)*colormap[x1][z1][i] + (1.0-v)*u*colormap[x2][z1][i] + v*(1.0-u)*colormap[x1][z2][i] + v*u*colormap[x2][z2][i]);
		}
		
		return val;
	}
	
	@Override
	public boolean isGlobalContained(double x, double z) {
		return extent.isContained(x, z);
	}
	
	@Override
	public float[] getGlobalColor(double x, double z) {
		x = (x - extent.getX()) / resolution;
		z = (z - extent.getZ()) / resolution;
		
		return colormap[(int) x][(int) z];
	}
	
	@Override
	public float[] getGlobalLerpColor(double x, double z) {
		double localX = (x - extent.getX()) / resolution;
		double localZ = (z - extent.getZ()) / resolution;
		
		return getLocalLerpColor(localX, localZ);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 255, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Colormap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeColormap dcm = new DatatypeColormap(extent, resolution);
		if (colormap != null) {
			float[][][] newMap = new float[mapPointsWidth][mapPointsLength][];
			for (int u = 0; u < mapPointsWidth; u++) {
				for (int v = 0; v < mapPointsLength; v++) {
					newMap[u][v] = colormap[u][v].clone();
				}
			}
			dcm.colormap = newMap;
		}
		return dcm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeColormap(new BuildExtent(x, y, z, width, height, length), resolution);
	}
}
