/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

/**
 * This class contains various methods used in relations to datatypes
 */
public class Datatypes {
	
	/**
	 * Verify that two datatypes are compatible for a connection
	 * 
	 * @param outputType the type of a module output
	 * @param inputType the type of a module input
	 * @return {@code true} if a connection with the given datatypes is valid
	 */
	public static boolean verifyCompatibleTypes(AbstractDatatype outputType, AbstractDatatype inputType) {
		if (inputType instanceof DatatypeNull || outputType instanceof DatatypeNull) {
			// Cannot verify that the types are of same type if one or both of them are of null(unknown) type
			return false;
		}
		else if (inputType instanceof DatatypeMultitype) {
			// The input type is multitype
			DatatypeMultitype multitype = (DatatypeMultitype) inputType;
			for (AbstractDatatype type: multitype.getDatatypes()) {
				if (type.getClass().equals(outputType.getClass())) {
					return true;
				}
			}
		}
		// Check if input type and output type are the same
		return inputType.getClass().equals(outputType.getClass());
	}
}
