/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.customobject;

public class CustomObject {
	/**
	 * Array storing block entries
	 */
	public Block[] blocks;
	
	/**
	 * Unit size of the object
	 */
	public int width, height, length;
	
	/**
	 * Minimum coordinate of voxels in the object
	 */
	public int xMin, yMin, zMin;
	/**
	 * Maximum coordinate of voxels in the object
	 */
	public int xMax, yMax, zMax;
	
	public CustomObject(Block[] blocks) {
		this.blocks = blocks;
		
		if (blocks == null) {
			return;
		}
		else if (blocks.length == 0) {
			return;
		}
		
		xMin = xMax = blocks[0].x;
		yMin = yMax = blocks[0].y;
		zMin = zMax = blocks[0].z;
		
		for (Block block: blocks) {
			//Check for min and max x
			if (block.x < xMin) {
				xMin = block.x;
			}
			else if (block.x > xMax) {
				xMax = block.x;
			}
			
			//Check for min and max y
			if (block.y < yMin) {
				yMin = block.y;
			}
			else if (block.y > yMax) {
				yMax = block.y;
			}
			
			//Check for min and max z
			if (block.z < zMin) {
				zMin = block.z;
			}
			else if (block.z > zMax) {
				zMax = block.z;
			}
		}
		
		width = xMax - xMin + 1;
		height = yMax - yMin + 1;
		length = zMax - zMin + 1;
	}
	
	public Block[] getBlocks() {
		return blocks;
	}
	
	public CustomObject clone() {
		return new CustomObject(getBlocks());
	}
}
