/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.worldsynth.addon.AddonLoader;
import net.worldsynth.color.WsColor;

public class MaterialRegistry {
	private static final Logger logger = LogManager.getLogger(MaterialRegistry.class);
		
	private static final HashMap<String, MaterialProfile<?, ?>> profiles = new HashMap<String, MaterialProfile<?, ?>>();
	
	public MaterialRegistry(File materialsDirectory, AddonLoader addonLoader) {
		logger.info("Initializing material registry");
		logger.info("Registering default material profile");
		registerProfile(new MaterialProfile("default"));
		
		logger.info("Registering material profiles from addons");
		for (MaterialProfile profile: addonLoader.getAddonMaterialProfiles()) {
			registerProfile(profile);
		}
		
		loadMaterials(materialsDirectory);
	}
	
	private static void registerProfile(MaterialProfile<?, ?> profile) {
		logger.info("Registering material profile of type \"" + profile.getClass().getName() + "\" and name \"" + profile.getProfileName() + "\"");
		profiles.put(profile.getProfileName(), profile);
	}
	
	public static MaterialProfile<?, ?> getProfile(String profileName) {
		if (!profiles.containsKey(profileName)) {
			logger.info("Attemted to acces profile with name: " + profileName + ", but it did not exist.");
			registerProfile(new MaterialProfile(profileName));
			logger.info("Created profile with name: " + profileName);
		}
		return profiles.get(profileName);
	}
	
	private void loadMaterials(File materialsDirectory) {
		logger.info("Loading materials from directory: " + materialsDirectory);
		if (!materialsDirectory.exists()) {
			logger.warn("Directory at " + materialsDirectory + " did not exist");
			return;
		}
		
		for (File sub: materialsDirectory.listFiles()) {
			if (sub.isDirectory()) {
				loadMaterials(sub);
			}
			else if (sub.getName().endsWith(".json")) {
				try {
					loadMaterialsFromFile(sub);
				} catch (IOException e) {
					logger.error("Problem occoured while trying to read material file: " + sub.getName(), e);
				}
			}
		}
	}
	
	private void loadMaterialsFromFile(File materialsFile) throws IOException {
		logger.info("Parsing materials from file: " + materialsFile);
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode materialsFileNode = objectMapper.readTree(materialsFile);
		
		Iterator<Entry<String, JsonNode>> iterator = materialsFileNode.fields();
		while (iterator.hasNext()) {
			Entry<String, JsonNode> entry = iterator.next();
			String profileName = entry.getKey();
			logger.info("Parsing materials into profile with name: " + profileName);
			if (entry.getValue().isObject()) {
				getProfile(profileName).parseMaterialsFromJsonNode((ObjectNode) entry.getValue(), materialsFile);
			}
		}
	}
	
	public static Material<?, ?> getMaterial(String idName) {
		for (MaterialProfile<?, ?> profile: profiles.values()) {
			Material<?, ?> m = profile.getMaterial(idName);
			if (m != null) {
				return m;
			}
		}
		
		logger.warn("Attempted to get material with idName: " + idName + ", but it did not exist. Creating placeholder material.");
		return new MaterialBuilder(idName, idName).color(WsColor.MAGENTA).createMaterial();
	}
	
	public static Material<?, ?> getMaterial(String idName, String profileName) {
		if (profiles.containsKey(profileName)) {
			MaterialProfile<?, ?> profile = getProfile(profileName);
			Material<?, ?> m = profile.getMaterial(idName);
			if (m != null) {
				return m;
			}
		}
		
		logger.warn("Attempted to get material with idName: " + idName + " from profile: " + profileName + ", but it did not exist.");
		return null;
	}
	
	public static Material<?, ?> getDefaultMaterial() {
		return profiles.containsKey("minecraft") ? getDefaultMaterial("minecraft") : getDefaultMaterial("default");
	}
	
	public static Material<?, ?> getDefaultMaterial(String profileName) {
		MaterialProfile<?, ?> profile = profiles.get(profileName);
		if (profile != null) {
			return profile.getDefaultMaterial();
		}
		return null;
	}

	public static ArrayList<Material<?, ?>> getMaterialsAlphabetically() {
		ArrayList<Material<?, ?>> materials = new ArrayList<Material<?, ?>>();
		for (MaterialProfile<?, ?> profile: profiles.values()) {
			materials.addAll(profile.getMaterials());
		}
		Collections.sort(materials);
		return materials;
	}
	
	public static ArrayList<Material<?, ?>> getMaterialsAlphabetically(String profileName) {
		if (!profiles.containsKey(profileName)) return null;
		ArrayList<Material<?, ?>> materials = new ArrayList<Material<?, ?>>(profiles.get(profileName).getMaterials());
		Collections.sort(materials);
		return materials;
	}
	
	public static MaterialState<?, ?> getMaterialState(String idName) {
		for (MaterialProfile<?, ?> profile: profiles.values()) {
			MaterialState<?, ?> s = profile.getMaterialState(idName);
			if (s != null) {
				return s;
			}
		}
		
		logger.warn("Attempted to get materialstate with idName: " + idName + ", but it did not exist. Creating placeholder materialstate.");
		
		MaterialBuilder materialBuilder = new MaterialBuilder(idName, idName);
		MaterialState<?, ?> placeholderMaterialState = materialBuilder.new MaterialStateBuilder(true).createMaterialState();
		materialBuilder.createMaterial();
		return placeholderMaterialState;
	}
	
	public static MaterialState<?, ?> getMaterialState(String idName, String profileName) {
		if (profiles.containsKey(profileName)) {
			MaterialProfile<?, ?> profile = getProfile(profileName);
			MaterialState<?, ?> s = profile.getMaterialState(idName);
			if (s != null) {
				return s;
			}
		}
		
		logger.warn("Attempted to get materialstate with idName: " + idName + " from profile: " + profileName + ", but it did not exist.");
		return null;
	}
	
	public static boolean isAir(MaterialState<?, ?> material) {
		if (material == null) return true;
		return material.isAir();
	}
	
	private static Material<?, ?> defaultAir;
	public static Material<?, ?> getDefaultAir() {
		if (defaultAir == null) {
			defaultAir = Material.NULL;
			for (MaterialProfile<?, ?> profile: profiles.values()) {
				for (Material<?, ?> material: profile.getMaterials()) {
					if (material.isAir()) {
						defaultAir = material;
						return defaultAir;
					}
				}
			}
		}
		return defaultAir;
	}
}
