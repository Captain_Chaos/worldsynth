/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import javafx.scene.paint.Color;
import net.worldsynth.color.WsColor;

public class MaterialBuilder<M extends Material<M, S>, S extends MaterialState<M, S>> {
	protected String idName;
	protected String displayName;
	protected WsColor color = WsColor.MAGENTA;
	protected Texture texture;
	protected HashSet<String> tags;
	protected boolean isAir = false;
	protected Map<String, List<String>> properties;
	protected Set<S> states;
	
	public MaterialBuilder(String idName, String displayName) {
		this.idName = idName;
		this.displayName = displayName;
	}
	
	public MaterialBuilder(String idName, JsonNode node, File materialsFile) {
		this.idName = idName;
		
		if (node.has("displayName")) {
			displayName = node.get("displayName").asText();
		}
		else {
			displayName = idName;
		}
		
		if (node.has("color")) {
			ArrayNode colorNode = (ArrayNode) node.get("color");
			int[] c = new int[colorNode.size()];
			for (int i = 0; i < colorNode.size(); i++) {
				c[i] = colorNode.get(i).asInt();
			}
			if (colorNode.size() == 3) color = new WsColor(c[0], c[1], c[2]);
			else if (colorNode.size() == 4) color = new WsColor(c[0], c[1], c[2], c[3]);
		}
		
		if (node.has("texture")) {
			texture = new Texture(materialsFile.getParent(), node.get("texture").asText());
		}
		
		if (node.has("tags")) {
			tags = new ObjectMapper().convertValue(node.get("tags"), new TypeReference<LinkedHashSet<String>>(){});
		}
		
		if (node.has("isAir")) {
			isAir = node.get("isAir").asBoolean();
		}
		
		if (node.has("properties")) {
			properties = new ObjectMapper().convertValue(node.get("properties"), new TypeReference<LinkedHashMap<String, List<String>>>(){});
		}
		
		if (node.has("states")) {
			parseMaterialStates(node.get("states"), materialsFile);
		}
	}
	
	protected void parseMaterialStates(JsonNode node, File materialsFile) {
		Iterator<JsonNode> iterator = node.iterator();
		while (iterator.hasNext()) {
			new MaterialStateBuilder(iterator.next(), materialsFile).createMaterialState();
		}
	}
	
	public MaterialBuilder<M, S> displayName(String displayName) {
		this.displayName = displayName;
		return this;
	}
	
	public MaterialBuilder<M, S> color(WsColor color) {
		this.color = color;
		return this;
	}
	
	public MaterialBuilder<M, S> color(Color color) {
		return color(new WsColor(color));
	}
	
	public MaterialBuilder<M, S> color(String colorstring) {
		return color(new WsColor(colorstring));
	}
	
	public MaterialBuilder<M, S> color(float red, float green, float blue) {
		return color(new WsColor(red, green, blue));
	}
	
	public MaterialBuilder<M, S> color(float red, float green, float blue, float opacity) {
		return color(new WsColor(red, green, blue, opacity));
	}
	
	public MaterialBuilder<M, S> color(int red, int green, int blue) {
		return color(new WsColor(red, green, blue));
	}
	
	public MaterialBuilder<M, S> color(int red, int green, int blue, int opacity) {
		return color(new WsColor(red, green, blue, opacity));
	}
	
	public MaterialBuilder<M, S> texture(Texture texture) {
		this.texture = texture;
		return this;
	}
	
	public MaterialBuilder<M, S> tags(String... tags) {
		this.tags = new HashSet<String>(Arrays.asList(tags));
		return this;
	}
	
	public MaterialBuilder<M, S> addTags(String... tags) {
		if (this.tags == null) {
			return tags(tags);
		}
		this.tags.addAll(Arrays.asList(tags));
		return this;
	}
	
	public MaterialBuilder<M, S> addTag(String tag) {
		return addTags(tag);
	}
	
	public MaterialBuilder<M, S> isAir(boolean isAir) {
		this.isAir = isAir;
		return this;
	}
	
	public MaterialBuilder<M, S> properties(Map<String, List<String>> properties) {
		this.properties = properties;
		return this;
	}
	
	public MaterialBuilder<M, S> addProperty(String name, String ... values) {
		return addProperty(name, new ArrayList<String>(Arrays.asList(values)));
	}
	
	public MaterialBuilder<M, S> addProperty(String name, List<String> values) {
		properties.put(name, values);
		return this;
	}
	
	protected MaterialBuilder<M, S> addState(S state) {
		if (this.states == null) {
			this.states = new LinkedHashSet<S>();
		}
		this.states.add(state);
		return this;
	}
	
	public M createMaterial() {
		extrapolateStates(isDefault -> {
			return new MaterialStateBuilder(isDefault);
		});
		return (M) new Material<M, S>(idName, displayName, color, texture, tags, isAir, properties, states);
	}
	
	/**
	 * Extrapolate and construct all remaining undeclared states for the material being built.
	 * @param getNewMaterialStateBuilder Lambda function for constructing new MaterialStateBuilder
	 */
	protected final void extrapolateStates(Function<Boolean, MaterialStateBuilder> getNewMaterialStateBuilder) {
		if (properties == null || properties.isEmpty()) {
			if (states == null || states.isEmpty()) {
				getNewMaterialStateBuilder.apply(true).createMaterialState();
			}
		}
		else {
			ArrayList<ArrayList<String>> possibleStates = cartesianPropertiesProduct(properties);
			HashMap<StateProperties, S> statesMap = new HashMap<StateProperties, S>();
			S defaultState = null;
			
			if (states != null && !states.isEmpty()) {
				for (S state: states) {
					StateProperties stateProperties = state.getProperties();
					statesMap.put(stateProperties, state);
					if (state.isDefault()) {
						defaultState = state;
					}
				}
			}
			for (ArrayList<String> stateValues: possibleStates) {
				MaterialStateBuilder stateBuilder = getNewMaterialStateBuilder.apply(false);
				//Put property values
				int i = 0;
				for (String propKey: properties.keySet()) {
					stateBuilder.addProperty(new Property(propKey, stateValues.get(i++)));
				}
				
				if (defaultState == null) {
					stateBuilder.isDefault(true);
				}
				
				if (!statesMap.containsKey(stateBuilder.properties)) {
					S generatedState = stateBuilder.createMaterialState();
					if (generatedState.isDefault()) {
						defaultState = generatedState;
					}
				}
			}
		}
	}
	
	private ArrayList<ArrayList<String>> cartesianPropertiesProduct(Map<String, List<String>> properties) {
		ArrayList<ArrayList<String>> product = new ArrayList<ArrayList<String>>();
		for (String propKey: properties.keySet()) {
			product = cartesianProduct(product, properties.get(propKey));
		}
		return product;
	}
	
	private ArrayList<ArrayList<String>> cartesianProduct(ArrayList<ArrayList<String>> a, List<String> b) {
		ArrayList<ArrayList<String>> product = new ArrayList<ArrayList<String>>();
		if (a == null || a.isEmpty()) {
			for (String bn: b) {
				ArrayList<String> anbn = new ArrayList<String>();
				anbn.add(bn);
				product.add(anbn);
			}
			return product;
		}
		
		for (ArrayList<String> an: a) {
			for (String bn: b) {
				ArrayList<String> anbn = new ArrayList<String>(an);
				anbn.add(bn);
				product.add(anbn);
			}
		}
		return product;
	}
	
	////////////////////////////
	// Material State Builder //
	////////////////////////////
	
	public class MaterialStateBuilder {
		protected StateProperties properties = new StateProperties();
		protected boolean isDefault;
		
		protected String displayName;
		protected WsColor color;
		protected Texture texture;
		
		public MaterialStateBuilder(boolean isDefault) {
			this.isDefault = isDefault;
		}
		
		public MaterialStateBuilder(JsonNode node, File materialsFile) {
			if (node.has("displayName")) {
				displayName = node.get("displayName").asText();
			}
			
			if (node.has("color")) {
				ArrayNode colorNode = (ArrayNode) node.get("color");
				int[] c = new int[colorNode.size()];
				for (int i = 0; i < colorNode.size(); i++) {
					c[i] = colorNode.get(i).asInt();
				}
				if (colorNode.size() == 3) color = new WsColor(c[0], c[1], c[2]);
				else if (colorNode.size() == 4) color = new WsColor(c[0], c[1], c[2], c[3]);
			}
			
			if (node.has("texture")) {
				texture = new Texture(materialsFile.getParent(), node.get("texture").asText());
			}
			
			if (node.has("properties")) {
				LinkedHashMap<String, String> properties = new ObjectMapper().convertValue(node.get("properties"), new TypeReference<LinkedHashMap<String, String>>(){});
				this.properties = new StateProperties(properties);
			}
			
			if (node.has("default")) {
				isDefault = node.get("default").asBoolean();
			}
		}
		
		public MaterialStateBuilder properties(StateProperties properties) {
			this.properties = properties;
			return this;
		}
		
		public MaterialStateBuilder addProperty(Property property) {
			this.properties.addProperty(property);
			return this;
		}
		
		public MaterialStateBuilder isDefault(boolean isDefault) {
			this.isDefault = isDefault;
			return this;
		}
		
		public MaterialStateBuilder displayName(String displayName) {
			this.displayName = displayName;
			return this;
		}
		
		public MaterialStateBuilder color(WsColor color) {
			this.color = color;
			return this;
		}
		
		public MaterialStateBuilder color(Color color) {
			return color(new WsColor(color));
		}
		
		public MaterialStateBuilder color(String colorstring) {
			return color(new WsColor(colorstring));
		}
		
		public MaterialStateBuilder color(float red, float green, float blue) {
			return color(new WsColor(red, green, blue));
		}
		
		public MaterialStateBuilder color(float red, float green, float blue, float opacity) {
			return color(new WsColor(red, green, blue, opacity));
		}
		
		public MaterialStateBuilder color(int red, int green, int blue) {
			return color(new WsColor(red, green, blue));
		}
		
		public MaterialStateBuilder color(int red, int green, int blue, int opacity) {
			return color(new WsColor(red, green, blue, opacity));
		}
		
		public MaterialStateBuilder texture(Texture texture) {
			this.texture = texture;
			return this;
		}
		
		public S createMaterialState() {
			S state = (S) new MaterialState<M, S>(properties, isDefault, displayName, color, texture);
			addState(state);
			return state;
		}
	}
}
