/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth;

import java.io.File;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorldSynthDirectoryConfig {
	private static final Logger logger = LogManager.getLogger(WorldSynthDirectoryConfig.class);
	
	private File worldSynthDirectory;
	
	private HashMap<String, File> directoryMap = new HashMap<String, File>();
	
	private File materialsDirectory;
	private File biomesDirectory;
	private File addonsDirectory;
	
	public WorldSynthDirectoryConfig(File worldSynthDirectory) {
		this(worldSynthDirectory, null, null, null);
	}
	
	public WorldSynthDirectoryConfig(File worldSynthDirectory, File addonDirectory, File materialsDirectory, File biomesDirectory) {
		this.worldSynthDirectory = worldSynthDirectory;
		this.addonsDirectory = initDirectory("addons", worldSynthDirectory, addonDirectory);
		this.materialsDirectory = initDirectory("materials", worldSynthDirectory, materialsDirectory);
		this.biomesDirectory = initDirectory("biomes", worldSynthDirectory, biomesDirectory);
	}
	
	protected final File initDirectory(String directoryName, File rootDirectory, File preferedDirectory) {
		logger.info("Initializing " + directoryName + " directory");
		if (preferedDirectory == null) {
			logger.info("No prefered directory was provided for " + directoryName);
			preferedDirectory = new File(rootDirectory, directoryName);
		}
		else {
			logger.info("Prefered directory povided for " + directoryName);
		}
		logger.info(directoryName + " directory at " + preferedDirectory.getAbsolutePath());
		if (!preferedDirectory.exists()) {
			logger.warn("Directory at " + preferedDirectory + " did not exist");
			preferedDirectory.mkdir();
			logger.warn("Directory created directory at " + preferedDirectory);
		}
		
		directoryMap.put(directoryName, preferedDirectory);
		
		return preferedDirectory;
	}
	
	public final File getDirectory(String directoryName) {
		if (directoryMap.containsKey(directoryName)) {
			return directoryMap.get(directoryName);
		}
		
		return initDirectory(directoryName, worldSynthDirectory, null);
	}
	
	public File getAddonDirectory() {
		return addonsDirectory;
	}
	
	public File getMaterialsDirectory() {
		return materialsDirectory;
	}
	
	public File getBiomesDirectory() {
		return biomesDirectory;
	}
}
