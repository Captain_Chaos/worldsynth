/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.build;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.ModuleWrapperIO;
import net.worldsynth.patch.Patch;
import net.worldsynth.util.event.build.BuildStatus.BuildState;
import net.worldsynth.util.event.build.BuildStatusEvent;
import net.worldsynth.util.event.build.BuildStatusListener;

public class Build {
	
	public static AbstractDatatype getModuleOutput(ModuleWrapper wrapper, ModuleOutputRequest request, BuildStatusListener buildListener) {
		Patch patch = wrapper.getParentPatch();
		
		if (buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.REGISTERED, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		if (buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.PREPARING_INPUTREQUESTS, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Get the input requests the module wants for building the output requested
		//and make sure all requests are unique instances.
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		for (Entry<String, ModuleInputRequest> entry: wrapper.module.getInputRequestsSuper(request).entrySet()) {
			if (entry == null) {
				continue;
			}
			inputRequests.put(entry.getKey(), new ModuleInputRequest(entry.getValue().getInput(), entry.getValue().getData().clone()));
		}
		
		if (buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.TRANSLATING_INPUTREQUESTS_TO_OUTPUTREQUESTS, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Translate the input requests to output requests
		HashMap<String, ModuleOutputRequest> outputRequests = new HashMap<String, ModuleOutputRequest>();
		HashMap<String, ModuleWrapper> outputRequestsModuleWrapper = new HashMap<String, ModuleWrapper>();
		for (Entry<String, ModuleInputRequest> entry: inputRequests.entrySet()) {
			//Transform inputrequests into output requests if possible
			ModuleWrapperIO input = wrapper.getWrapperIoByModuleIo(entry.getValue().getInput());
			List<ModuleConnector> connectors = patch.getModuleConnectorsByWrapperIo(input);
			if (connectors.isEmpty()) {
				continue;
			}
			ModuleConnector connector = connectors.get(0);
			ModuleWrapper requestedWrapper = connector.module1;
			ModuleOutput requestedModuleOutput = (ModuleOutput) connector.module1Io.getIO();
			
			ModuleOutputRequest newOutputRequest = new ModuleOutputRequest(requestedModuleOutput, entry.getValue().getData());
			if (entry.getValue().getData() instanceof DatatypeMultitype) {
				for (AbstractDatatype datatype: ((DatatypeMultitype) entry.getValue().getData()).getDatatypes()) {
					if (requestedModuleOutput.getData().getClass().equals(datatype.getClass())) {
						newOutputRequest = new ModuleOutputRequest(requestedModuleOutput, datatype);
						break;
					}
				}
			}
			outputRequests.put(entry.getKey(), newOutputRequest);
			outputRequestsModuleWrapper.put(entry.getKey(), requestedWrapper);
		}
		
		if (buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.PREPARING_INPUT_DATA, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Invoke the outputrequests
		HashMap<String, AbstractDatatype> data = new HashMap<String, AbstractDatatype>();
		for (Entry<String, ModuleOutputRequest> entry: outputRequests.entrySet()) {
			AbstractDatatype newData = getModuleOutput(outputRequestsModuleWrapper.get(entry.getKey()), entry.getValue(), buildListener);
			if (newData != null) {
				data.put(entry.getKey(), newData);
			}
		}
		
		if (buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.BUILDING, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Build the module wrapped
		AbstractDatatype output = wrapper.module.buildModuleSuper(data, request);
		
		if (buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.DONE_BUILDING, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Return the data from the build
		return output;
	}
}
