/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.fileio;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.FileParameter;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class ModuleColormapImporter extends AbstractModule {
	
	private FileParameter colormapImageFile = new FileParameter("filepath", "Heightmap file", null, null, false, false, new ExtensionFilter("PNG", "*.png"));
	private DoubleParameter xOffset = new DoubleParameter("xoffset", "X offset", null, 0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1000, 1000);
	private DoubleParameter zOffset = new DoubleParameter("zoffset", "Z offset", null, 0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1000, 1000);
	private BooleanParameter cache = new BooleanParameter("cache", "Cache colormap", null, true);
	
	private float[][][] cachedColormap = null;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				colormapImageFile,
				xOffset,
				zOffset,
				cache
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;int mpw = requestData.mapPointsWidth;
		
		int mpl = requestData.mapPointsLength;
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		
		//----------READ INPUTS----------//
		
		double xOffset = this.xOffset.getValue();
		double zOffset = this.zOffset.getValue();
		File colormapImageFile = this.colormapImageFile.getValue();
		
		if (colormapImageFile == null || !colormapImageFile.exists()) {
			return null;
		}
		
		float[][][] fileInputMap = cachedColormap;
		if (cache.getValue() && cachedColormap == null) {
			fileInputMap = cachedColormap = readeImageFromFile(colormapImageFile);
		}
		else if (!cache.getValue()) {
			fileInputMap = readeImageFromFile(colormapImageFile);
		}
		
		int inputMapWidth = fileInputMap.length;
		int inputMapHeight = fileInputMap[0].length;
		
		//----------BUILD----------//
		
		float[][][] map = new float[mpw][mpl][3];
		for (int u = 0; u < mpw; u++) {
			int lx = (int) Math.floor((double) u * res + x - xOffset);
			if (lx < 0 || lx >= inputMapWidth) {
				continue;
			}
			for (int v = 0; v < mpl; v++) {
				int lz = (int) Math.floor((double) v * res + z - zOffset);
				if (lz < 0 || lz >= inputMapHeight) {
					continue;
				}
				map[u][v] = fileInputMap[lx][lz];
			}
		}
		
		requestData.setColormap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Colormap importer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.IMPORTER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Colormap")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ParameterUiElement<Double> parameterX = xOffset.getUi();
		ParameterUiElement<Double> parameterZ = zOffset.getUi();
		ParameterUiElement<File> parameterHeightmatFile = colormapImageFile.getUi();
		ParameterUiElement<Boolean> parameterCache = cache.getUi();
		
		parameterX.addToGrid(pane, 0);
		parameterZ.addToGrid(pane, 1);
		parameterHeightmatFile.addToGrid(pane, 2);
		parameterCache.addToGrid(pane, 3);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			parameterHeightmatFile.applyUiValue();
			parameterCache.applyUiValue();
			parameterX.applyUiValue();
			parameterZ.applyUiValue();
			cachedColormap = null;
		};
		
		return applyHandler;
	}
	
	private float[][][] readeImageFromFile(File f) {
		float[][][] heightmap = null;
		
		try {
			BufferedImage image = ImageIO.read(f);
			
			heightmap = new float[image.getWidth()][image.getHeight()][3];
			for (int x = 0; x < heightmap.length; x++) {
				for (int y = 0; y < heightmap[0].length; y++) {
					Color c = new Color(image.getRGB(x, y));
					heightmap[x][y][0] = (float) c.getRed() / 255.0F;
					heightmap[x][y][1] = (float) c.getGreen() / 255.0F;
					heightmap[x][y][2] = (float) c.getBlue() / 255.0F;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return heightmap;
	}
}
