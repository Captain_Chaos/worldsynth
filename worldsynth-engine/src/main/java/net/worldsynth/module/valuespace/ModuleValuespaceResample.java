package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleValuespaceResample extends AbstractModule {
	
private IntegerParameter resamples = new IntegerParameter("resamples", "Resamples", null, 8, 1, Integer.MAX_VALUE, 1, 32);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				resamples
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeValuespace i1 = (DatatypeValuespace) inputs.get("input");
		
		//----------BUILD----------//
		
		float[][][] values = new float[spw][sph][spl];
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					values[u][v][w] = i1.getGlobalLerpValue(x + u*res, y + v*res, z + w*res);
				}
			}
		}
		
		requestData.setValuespace(values);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeValuespace ord = (DatatypeValuespace) outputRequest.data;
		double res = Math.max(resamples.getValue(), ord.resolution);
		BuildExtent sampleExtent = getSampleExtent(ord.extent, resamples.getValue());
		
		DatatypeValuespace ird = new DatatypeValuespace(sampleExtent, res);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), ird));
		
		return inputRequests;
	}
	
	private BuildExtent getSampleExtent(BuildExtent extent, double sampleGridSize) {
		double x1 = Math.floor(extent.getX() / sampleGridSize);
		x1 *= sampleGridSize;
		double y1 = Math.floor(extent.getY() / sampleGridSize);
		y1 *= sampleGridSize;
		double z1 = Math.floor(extent.getZ() / sampleGridSize);
		z1 *= sampleGridSize;

		double x2 = Math.ceil((extent.getX() + extent.getWidth()) / sampleGridSize) + 1.0;
		x2 *= sampleGridSize;
		double y2 = Math.ceil((extent.getY() + extent.getHeight()) / sampleGridSize) + 1.0;
		y2 *= sampleGridSize;
		double z2 = Math.ceil((extent.getZ() + extent.getLength()) / sampleGridSize) + 1.0;
		z2 *= sampleGridSize;
		
		double width = x2 - x1;
		double height = y2 - y1;
		double length = z2 - z1;
		
		return new BuildExtent(x1, y1, z1, width, height, length);
	}

	@Override
	public String getModuleName() {
		return "Resample";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
