/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.util.math.FastTrigonometry;
import net.worldsynth.util.math.MathHelperScalar;

public class ModuleValuespaceDistort extends AbstractModule {
	
	private DoubleParameter distortion = new DoubleParameter("distortion", "Distortion", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 100.0);
	private EnumParameter<DistortionQuality> distortionQuality = new EnumParameter<DistortionQuality>("distortionquality", "Distortion quality", null, DistortionQuality.class, DistortionQuality.LERP);
	private EnumParameter<InputSampling> inputSampling = new EnumParameter<InputSampling>("inputsampling", "Input sampling", null, InputSampling.class, InputSampling.LERP);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				distortion,
				distortionQuality,
				inputSampling
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeValuespace ord = (DatatypeValuespace) outputRequest.data;
		
		// Expand in whole sample distance lengths
		double distortionExpansion = Math.ceil(Math.abs(this.distortion.getValue()) / ord.resolution) * ord.resolution;
		DatatypeValuespace valuespaceRequestData = new DatatypeValuespace(BuildExtent.expandedBuildExtent(ord.extent, distortionExpansion, 0.0, distortionExpansion), ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), valuespaceRequestData));
		
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), ord));
		inputRequests.put("distortionamplitude", new ModuleInputRequest(getInput("Distortion amplitude"), valuespaceRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		double distortion = this.distortion.getValue();
		DistortionQuality distortionQuality = this.distortionQuality.getValue();
		InputSampling inputSampling = this.inputSampling.getValue();
		
		// Check necessary inputs
		if (inputs.get("input") == null || inputs.get("distortion") == null) {
			// If the main input or distortion input is null, there is not enough input and then just return null
			return null;
		}
		
		// Read in input
		DatatypeValuespace input = (DatatypeValuespace) inputs.get("input");
		
		// Read in distortion
		float[][][][] distortionSpace = new float[2][spw][sph][spl];
		DatatypeValuespace distortionInput = (DatatypeValuespace) inputs.get("distortion");
		if (distortionQuality == DistortionQuality.PRECISE) {
			// Precise distortion
			for (int u = 0; u < spw; u++) {
				for (int v = 0; v < sph; v++) {
					for (int w = 0; w < spl; w++) {
						distortionSpace[0][u][v][w] = (float) Math.cos(distortionInput.getLocalValue(u, v, w) * Math.PI * 2.0);
						distortionSpace[1][u][v][w] = (float) Math.sin(distortionInput.getLocalValue(u, v, w) * Math.PI * 2.0);
					}
				}
			}
		}
		else if (distortionQuality == DistortionQuality.LERP) {
			// Lerped distortion
			for (int u = 0; u < spw; u++) {
				for (int v = 0; v < sph; v++) {
					for (int w = 0; w < spl; w++) {
						distortionSpace[0][u][v][w] = (float) FastTrigonometry.COS.applyLerp(distortionInput.getLocalValue(u, v, w) * Math.PI * 2.0);
						distortionSpace[1][u][v][w] = (float) FastTrigonometry.SIN.applyLerp(distortionInput.getLocalValue(u, v, w) * Math.PI * 2.0);
					}
				}
			}
		}
		else {
			// Discrete distortion
			for (int u = 0; u < spw; u++) {
				for (int v = 0; v < sph; v++) {
					for (int w = 0; w < spl; w++) {
						distortionSpace[0][u][v][w] = (float) FastTrigonometry.COS.applyDiscrete(distortionInput.getLocalValue(u, v, w) * Math.PI * 2.0);
						distortionSpace[1][u][v][w] = (float) FastTrigonometry.SIN.applyDiscrete(distortionInput.getLocalValue(u, v, w) * Math.PI * 2.0);
					}
				}
			}
		}
		
		// Read in distortion amplitude modifier
		DatatypeValuespace distortionAmplitudeInput = null;
		if (inputs.get("distortionamplitude") != null) {
			distortionAmplitudeInput = (DatatypeValuespace) inputs.get("distortionamplitude");
		}
		
		//----------BUILD----------//
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		double samplesOffset = Math.ceil(Math.abs(distortion) / requestData.resolution);
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					double localDistortion = distortion;
					if (distortionAmplitudeInput != null) {
						localDistortion *= MathHelperScalar.clamp(distortionAmplitudeInput.getLocalValue(u, v, w), -1.0, 1.0);
					}
					
					double xSampleDistortion = distortionSpace[0][u][v][w]*localDistortion / requestData.resolution;
					double zSampleDistortion = distortionSpace[1][u][v][w]*localDistortion / requestData.resolution;
					
					float o = 0.0f;
					if (inputSampling == InputSampling.LERP) {
						o = input.getLocalLerpValue((u+samplesOffset + xSampleDistortion), v, (w+samplesOffset + zSampleDistortion));
					}
					else {
						o = input.getLocalValue((int) (u+samplesOffset + xSampleDistortion), v, (int) (w+samplesOffset + zSampleDistortion));
					}
					
					valuespace[u][v][w] = o;
				}
			}
		}
		
		requestData.setValuespace(valuespace);
		
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Distort";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Input"),
				new ModuleInput(new DatatypeValuespace(), "Distortion"),
				new ModuleInput(new DatatypeValuespace(), "Distortion amplitude")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	private enum DistortionQuality {
		PRECISE, LERP, DISCRETE;
	}
	
	private enum InputSampling {
		LERP, DISCRETE;
	}
}
