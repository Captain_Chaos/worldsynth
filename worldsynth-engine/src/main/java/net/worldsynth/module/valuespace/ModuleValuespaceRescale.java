package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatTupleParameter;

public class ModuleValuespaceRescale extends AbstractModule {
	
	FloatTupleParameter inputRange = new FloatTupleParameter("inputrange", "Input range", null, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, 0.0f, 1.0f);
	FloatTupleParameter outputRange = new FloatTupleParameter("outputrange", "Output range", null, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, 0.0f, 1.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				inputRange,
				outputRange
		};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		float i0 = inputRange.getValue().getValue(0);
		float i1 = inputRange.getValue().getValue(1);
		float o0 = outputRange.getValue().getValue(0);
		float o1 = outputRange.getValue().getValue(1);
		
		//Read in input
		if (inputs.get("input") == null) {
			// If the input is null, there is no input and then just return null
			return null;
		}
		float[][][] inputSpace = ((DatatypeValuespace) inputs.get("input")).getValuespace();
		
		//----------BUILD----------//
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					valuespace[u][v][w] = map(inputSpace[u][v][w], i0, i1, o0, o1);
				}
			}
		}
		
		requestData.setValuespace(valuespace);
		
		return requestData;
	}

	private float map(float x, float i0, float i1, float o0, float o1) {
		return (x - i0) * (o1 - o0) / (i1 - i0) + o0;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Rescale";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
