/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleValuespaceCombiner extends AbstractModule {
	
	private EnumParameter<Operation> operation = new EnumParameter<Operation>("operation", "Arithmetic operation", null, Operation.class, Operation.ADDITION);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				operation
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		Operation operation = this.operation.getValue();
		
		if (inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		float[][][] inputMap0 = ((DatatypeValuespace) inputs.get("primary")).getValuespace();
		float[][][] inputMap1 = ((DatatypeValuespace) inputs.get("secondary")).getValuespace();
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					float i0 = inputMap0[u][v][w];
					float i1 = inputMap1[u][v][w];
					float o = 0.0f;
					
					switch (operation) {
					case ADDITION:
						o = i0 + i1;
						break;
					case SUBTRACTION:
						o = i0 - i1;
						break;
					case MULTIPLICATION:
						o = i0 * i1;
						break;
					case DIVISION:
						o = i0 / i1;
						break;
					case MODULO:
						o = i0 % i1;
						break;
					case AVERAGE:
						o = (i0 + i1) / 2;
						break;
					case MAX:
						o = Math.max(i0, i1);
						break;
					case MIN:
						o = Math.min(i0, i1);
						break;
					case OWERFLOW:
						o = (i0 + i1);
						o = o % 1.0f;
						break;
					case UNDERFLOW:
						o = (i0 - i1);
						o = o < 0.0f ? o + 1.0f : o;
						break;
					}
					
					valuespace[u][v][w] = o;
				}
			}
		}
		
		requestData.setValuespace(valuespace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(1), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Valuespace combiner";
	}
	
	@Override
	public String getModuleMetaTag() {
		return operation.getValue().name().substring(0, 3);
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Primary"),
				new ModuleInput(new DatatypeValuespace(), "Secondary")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

	private enum Operation {
		ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, MODULO, AVERAGE, MAX, MIN, OWERFLOW, UNDERFLOW;
	}
}
