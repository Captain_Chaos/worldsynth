/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.*;
import net.worldsynth.util.Tuple;
import net.worldsynth.util.gen.Permutation;

public class ModuleValuespaceSimplePerlin extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleTupleParameter scale = new DoubleTupleParameter("scale", "Scale", null, 0.0, Double.POSITIVE_INFINITY, 100.0, 100.0, 100.0);
	private DoubleParameter amplitude = new DoubleParameter("amplitude", "Amplitude", null, 1.0, 0.0, Double.MAX_VALUE, 0.0, 5.0);
	private DoubleParameter offset = new DoubleParameter("offset", "Offset", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1.0, 1.0);
	private EnumParameter<Shape> shape = new EnumParameter<Shape>("shape", "Shape", null, Shape.class, Shape.STANDARD);
	
	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, permutationSize, 1);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				amplitude,
				offset,
				shape,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;

		Tuple<Double> scale = this.scale.getValue();
		double amplitude = this.amplitude.getValue();
		double offset = this.offset.getValue();
		Shape shape = this.shape.getValue();
		
		float[][][] values = new float[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					values[u][v][w] = (float) getValueAt(x+u*res, y+v*res, z+w*res, scale, shape, amplitude, offset);
				}
			}
		}
		
		requestData.setValuespace(values);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}
	
	public double getValueAt(double x, double y, double z, Tuple<Double> scale, Shape shape, double amplitude, double offset) {
		return perlin3d(x/scale.getValue(0), y/scale.getValue(1), z/scale.getValue(2), shape, amplitude, offset);
	}
	
	private double perlin3d(double x, double y, double z, Shape shape, double amplitude, double offset) {
		
		if (repeat > 0) {
			if (x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if (y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
			if (z < 0) {
				z = repeat+(z%repeat);
			}
			else {
				z = z%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		int zi = (int)z & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		double zf = z - (int)z;
		
		double u = easeCurve(xf);
		double v = easeCurve(yf);
		double w = easeCurve(zf);
		
		// zxy _ a=0, b=1
		int aaa, aab, aba, abb, baa, bab, bba, bbb;
		aaa = permutation.lHash(0, xi     , yi     , zi     );
		baa = permutation.lHash(0, inc(xi), yi     , zi     );
		aba = permutation.lHash(0, xi     , inc(yi), zi     );
		bba = permutation.lHash(0, inc(xi), inc(yi), zi     );
		aab = permutation.lHash(0, xi     , yi     , inc(zi));
		bab = permutation.lHash(0, inc(xi), yi     , inc(zi));
		abb = permutation.lHash(0, xi     , inc(yi), inc(zi));
		bbb = permutation.lHash(0, inc(xi), inc(yi), inc(zi));
		
		double a1, a2, a3, a4, a5, a6, a7, a8;
		a1 = grad(aaa, xf  , yf  , zf);
		a2 = grad(baa, xf-1, yf, zf);
		a3 = grad(aba, xf, yf-1, zf);
		a4 = grad(bba, xf-1, yf-1, zf);
		a5 = grad(aab, xf  , yf  , zf-1);
		a6 = grad(bab, xf-1, yf, zf-1);
		a7 = grad(abb, xf, yf-1, zf-1);
		a8 = grad(bbb, xf-1, yf-1, zf-1);
		
		double a12, a34, a56, a78, a1234, a5678;
		a12 = lerp(a1, a2, u);
		a34 = lerp(a3, a4, u);
		a56 = lerp(a5, a6, u);
		a78 = lerp(a7, a8, u);
		
		a1234 = lerp(a12, a34, v);
		a5678 = lerp(a56, a78, v);
		
		double value = lerp(a1234, a5678, w);
		
		switch (shape) {
		case STANDARD:
			value *= amplitude;
			value += offset;
			break;
		case RIDGED:
			value = 1.0f - Math.abs(value);
			value *= amplitude;
			value += offset;
			break;
		case BOWLY:
			value = Math.abs(value);
			value *= amplitude;
			value += offset;
			break;
		}
		
		return value;
	}
	
	private int inc(int num) {
		num++;
		int ret;
		if (num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}
	
	private double easeCurve(double t) {
		return t * t * t * (t * (t * 6 - 15) + 10);
	}
	
	private double grad(int hash, double x, double y, double z) {
		switch (hash & 0xF)
	    {
		    case 0x0: return  x + y;
	        case 0x1: return -x + y;
	        case 0x2: return  x - y;
	        case 0x3: return -x - y;
	        case 0x4: return  x + z;
	        case 0x5: return -x + z;
	        case 0x6: return  x - z;
	        case 0x7: return -x - z;
	        case 0x8: return  y + z;
	        case 0x9: return -y + z;
	        case 0xA: return  y - z;
	        case 0xB: return -y - z;
	        case 0xC: return  y + x;
	        case 0xD: return -y + z;
	        case 0xE: return  y - x;
	        case 0xF: return -y - z;
	        default: return 0; // never happens
	    }
	}
	
	private double lerp(double a, double b, double x) {
		return a + x * (b - a);
	}

	@Override
	public String getModuleName() {
		return "Simpel perlin";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	private enum Shape {
		STANDARD, RIDGED, BOWLY;
	}
}
