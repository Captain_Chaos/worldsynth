/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import javafx.scene.paint.Color;

public enum ModuleCategory implements IModuleCategory {
	
	/**
	 * Used for unknown modules where the module could not be recognised.
	 * </p>
	 * 
	 * This is for modules that cannot be identified when opening a project from a
	 * saved file because the module is not present in the module library. For
	 * modules that do not fall into any other of the given standard categories,
	 * create a new enumeration implementing {@link IModuleCategory}.
	 */
	UNKNOWN {
		@Override
		public Color classColor() {
			return Color.rgb(50, 50, 50);
		}
	},

	/////////////
	// GENERIC //
	/////////////
	
	GENERATOR {
		@Override
		public Color classColor() {
			return Color.rgb(100, 200, 100);
		}
	},
	MODIFIER {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	COMBINER {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	SELECTOR {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},

	/////////////////
	// MACRO STUFF //
	/////////////////
	
	MACRO {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}

	},
	MACRO_ENTRY {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}

	},
	MACRO_EXIT {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}

	},
	
	///////////////////
	// FILE IO STUFF //
	///////////////////

	EXPORTER {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}
	},
	IMPORTER {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}
	};
}
