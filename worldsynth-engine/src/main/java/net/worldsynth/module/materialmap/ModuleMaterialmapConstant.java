/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.materialmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.MaterialStateParameter;

public class ModuleMaterialmapConstant extends AbstractModule {
	
	private MaterialStateParameter material = new MaterialStateParameter("constant", "Material", null, MaterialRegistry.getDefaultMaterial().getDefaultState());
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				material
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeMaterialmap requestData = (DatatypeMaterialmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		MaterialState<?, ?> material = this.material.getValue();
		
		//----------BUILD----------//
		
		MaterialState<?, ?>[][] materialmap = new MaterialState<?, ?>[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				materialmap[u][v] = material;
			}
		}
		
		requestData.setMaterialmap(materialmap);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Constant materialmap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeMaterialmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
