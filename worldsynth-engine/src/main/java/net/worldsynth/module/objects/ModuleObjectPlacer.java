/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.customobject.CustomObjectFormatRegistry;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.datatype.DatatypeFeaturespace;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.featurepoint.Featurepoint3D;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.IntegerParameter;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class ModuleObjectPlacer extends AbstractModule {
	
	private final LayerManager layerManager = new LayerManager();
	private IntegerParameter placementRadius = new IntegerParameter("placementradius", "Placement radius", null, 16, 0, Integer.MAX_VALUE, 0, 32);
	private BooleanParameter placeAir = new BooleanParameter("placeair", "Place air", null, false);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace) outputRequest.data;
		BuildExtent inputExtent = BuildExtent.expandedBuildExtent(ord.extent, placementRadius.getValue(), 0.0, placementRadius.getValue());
		
		DatatypeBlockspace blockspaceInput = new DatatypeBlockspace(inputExtent, ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), blockspaceInput));
		
		for (ObjectLayer layer: layerManager.getLayers()) {
			if (!layer.isActive()) {
				continue;
			}
			AbstractDatatype placementRequestData = new DatatypeMultitype(new DatatypeFeaturemap(inputExtent, ord.resolution), new DatatypeFeaturespace(inputExtent, ord.resolution));
			inputRequests.put(layer.getName() + " placement", new ModuleInputRequest(getInput(layer.getName() + " placement"), placementRequestData));
		}
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		BuildExtent extent = requestData.extent;
		double y = requestData.extent.getY();
		double res = requestData.resolution;
		
		//----------READ INPUTS----------//
		
		DatatypeBlockspace inputBlockspace = (DatatypeBlockspace) inputs.get("input");
		
		boolean placeAir = this.placeAir.getValue();
		
		//----------BUILD----------//
		
		DatatypeBlockspace outputBlockspace = extractSubspace(inputBlockspace, (DatatypeBlockspace) request.data);
		
		// Iterate on all the layers
		for (ObjectLayer layer: layerManager.getLayers()) {
			if (!layer.isActive()) {
				continue;
			}
			if (inputs.get(layer.name + " placement") == null) {
				continue;
			}
			
			Featurepoint3D[] featurepoints = null;
			if (inputs.get(layer.name + " placement") instanceof DatatypeFeaturemap) {
				DatatypeFeaturemap featuremap = (DatatypeFeaturemap) inputs.get(layer.name + " placement");
				Featurepoint2D[] featuremapPoints = featuremap.getFeaturepoints();
				
				// Convert Featurepoint2D to Featurepoint3D
				featurepoints = new Featurepoint3D[featuremapPoints.length];
				for (int i = 0; i < featurepoints.length; i++) {
					if (inputBlockspace == null) {
						// Place feature at the bottom of the output extent if no blockspace input is provided
						featurepoints[i] = new Featurepoint3D(
								featuremapPoints[i].getX(),
								y,
								featuremapPoints[i].getZ(),
								featuremapPoints[i].getSeed());
					}
					else {
						// Place features at the surface block when a blockspace input is provided
						featurepoints[i] = new Featurepoint3D(
								featuremapPoints[i].getX(),
								getBlockspaceSurfaceHeightAt(inputBlockspace, featuremapPoints[i].getX(), featuremapPoints[i].getZ()),
								featuremapPoints[i].getZ(),
								featuremapPoints[i].getSeed());
					}
				}
			}
			else if (inputs.get(layer.name + " placement") instanceof DatatypeFeaturespace) {
				DatatypeFeaturespace featurespace = (DatatypeFeaturespace) inputs.get(layer.name + " placement");
				featurepoints = featurespace.getFeaturepoints();
			}
			
			if (layer.hasInternalObjects()) {
				// Place objects from layer object set
				for (Featurepoint3D point : featurepoints) {
					// Get an object from the layer cached objects according to the feature seed
					CustomObject object = layer.getCachedObjects().get((int) (point.getSeed() % layer.getCachedObjects().size()));
					// Create a located object
					LocatedCustomObject locatedObject = new LocatedCustomObject(point.getX(), point.getY(), point.getZ(), point.getSeed(), object);
					// Place the object into the blockspace
					placeObjectInBlockspace(outputBlockspace, locatedObject, placeAir);
				}
			}
			else {
				// Request a set of objects from the layer objects input
				LocatedCustomObject[] requestObjects = new LocatedCustomObject[featurepoints.length];
				for (int i = 0; i < featurepoints.length; i++) {
					//Create a located custom object for use in the objects input request
					requestObjects[i] = new LocatedCustomObject(featurepoints[i].getX(), featurepoints[i].getY(), featurepoints[i].getZ(), featurepoints[i].getSeed());
				}
				// Request the objects to be placed at by this layer
				BuildExtent objectsExtent = BuildExtent.expandedBuildExtent(extent, placementRadius.getValue(), 0.0, placementRadius.getValue());
				DatatypeObjects layerObjects = (DatatypeObjects) buildInputData(new ModuleInputRequest(getInput(layer.getName() + " objects"), new DatatypeObjects(requestObjects, objectsExtent, res)));
				if (layerObjects == null) {
					continue;
				}
				
				// Place objects
				for (LocatedCustomObject locatedObject: layerObjects.getLocatedObjects()) {
					placeObjectInBlockspace(outputBlockspace, locatedObject, placeAir);
				}
			}
		}
		
		return outputBlockspace;
	}
	
	private DatatypeBlockspace extractSubspace(DatatypeBlockspace parentSpace, DatatypeBlockspace subspace) {
		MaterialState<?, ?>[][][] subspaceMaterials = new MaterialState<?, ?>[subspace.spacePointsWidth][subspace.spacePointsHeight][subspace.spacePointsLength];
		
		MaterialState<?, ?> defaultAir = MaterialRegistry.getDefaultAir().getDefaultState();
		
		int blockspaceExtension = (int) Math.ceil(placementRadius.getValue()/subspace.resolution);
		
		for (int u = 0; u < subspace.spacePointsWidth; u++) {
			for (int v = 0; v < subspace.spacePointsHeight; v++) {
				for (int w = 0; w < subspace.spacePointsLength; w++) {
					if (parentSpace == null) {
						subspaceMaterials[u][v][w] = defaultAir;
					}
					else {
						subspaceMaterials[u][v][w] = parentSpace.getLocalMaterial(u+blockspaceExtension, v, w+blockspaceExtension);
					}
				}
			}
		}
		
		subspace.setBlockspace(subspaceMaterials);
		return subspace;
	}
	
	private double getBlockspaceSurfaceHeightAt(DatatypeBlockspace blockspace, double x, double z) {
		// Convert global coordinates to local coordinates
		x -= blockspace.extent.getX();
		x /= blockspace.resolution;
		int ix = (int) Math.floor(x);
		
		z -= blockspace.extent.getZ();
		z /= blockspace.resolution;
		int iz = (int) Math.floor(z);
		
		// Find local surface elevation
		int iy = blockspace.spacePointsHeight-1;
		while (blockspace.getLocalMaterial(ix, iy, iz).isAir() && iy > 0) {
			iy--;
		}
		
		// Convert local elevation to global elevation
		double y = iy;
		y *= blockspace.resolution;
		y += blockspace.extent.getY();
		return y;
	}
	
	private void placeObjectInBlockspace(DatatypeBlockspace blockspace, LocatedCustomObject locatedObject, boolean placeAir) {
		double x = locatedObject.getX();
		double y = locatedObject.getY();
		double z = locatedObject.getZ();
		Block[] blocks = locatedObject.getObject().getBlocks();
		
		double blockspaceX = blockspace.extent.getX();
		double blockspaceY = blockspace.extent.getY();
		double blockspaceZ = blockspace.extent.getZ();
		double blockspaceRes = blockspace.resolution;
		
		int spw = blockspace.spacePointsWidth;
		int sph = blockspace.spacePointsHeight;
		int spl = blockspace.spacePointsLength;
		
		// Convert global object position to local blockspace coordinates
		x -= blockspaceX;
		x /= blockspaceRes;
		
		y -= blockspaceY;
		y /= blockspaceRes;
		
		z -= blockspaceZ;
		z /= blockspaceRes;
		
		// Iterate trough the blocks
		for (Block b: blocks) {
			MaterialState<?, ?> material = b.material;
			if (!placeAir && material.isAir()) continue;
			
			// Convert object block position to local blockspace coordinates
			double bx = ((double) b.x) / blockspaceRes + x;
			double by = ((double) b.y) / blockspaceRes + y;
			double bz = ((double) b.z) / blockspaceRes + z;
			
			int ix = (int) Math.floor(bx);
			int iy = (int) Math.floor(by);
			int iz = (int) Math.floor(bz);
			
			// Ignore blocks that land outside of the blockspace
			if (ix < 0 || iy < 0 || iz < 0 || ix >= spw || iy >= sph || iz >= spl) {
				continue;
			}
			
			blockspace.getBlockspace()[ix][iy][iz] = material;
		}
	}
	
	@Override
	public String getModuleName() {
		return "Objects placer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ArrayList<ModuleInput> inputs = new ArrayList<ModuleInput>();
		inputs.add(new ModuleInput(new DatatypeBlockspace(), "Blockspace"));
		for (ObjectLayer layer: layerManager.getLayers()) {
			inputs.add(new ModuleInput(new DatatypeMultitype(new DatatypeFeaturemap(), new DatatypeFeaturespace()), layer.getName() + " placement"));
			if (!layer.hasInternalObjects()) {
				inputs.add(new ModuleInput(new DatatypeObjects(), layer.getName() + " objects"));
			}
		}
		
		return inputs.toArray(new ModuleInput[inputs.size()]);
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

	@Override
	protected EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ListView<ObjectLayer> layerListView = new ListView<ObjectLayer>();
		ListView<File> objectListView = new ListView<>();
		
		ParameterUiElement<Boolean> parameterPlaceAir = placeAir.getUi();
		parameterPlaceAir.addToGrid(pane, 0);
		
		ParameterUiElement<Integer> parameterPlacementRadius = placementRadius.getUi();
		parameterPlacementRadius.addToGrid(pane, 1);
		
		///////////////////////
		// Layer list editor //
		///////////////////////
		
		pane.add(new Label("Layers"), 0, 2);
		
		layerListView.setPrefSize(300, 400);
		layerListView.setCellFactory(param -> {
			LayerCell cell = new LayerCell(layerListView);
			cell.setOnEdit(e -> {
				layerListView.getSelectionModel().select(cell.layer);
			});
			return cell;
		});
		layerListView.getItems().setAll(layerManager.getEditorLayers());
		
		ScrollPane layerListPane = new ScrollPane(layerListView);
		GridPane.setColumnSpan(layerListPane, 2);
		pane.add(layerListPane, 0, 3);
		
		Button addLayerButton = new Button("Add layer");
		addLayerButton.setOnAction(e -> {
			layerListView.getItems().add(new ObjectLayer("New Layer"));
		});
		pane.add(addLayerButton, 0, 4);
		
		////////////////////////////
		// Layer file list editor //
		////////////////////////////
		
		Button addObjectsButton = new Button("Add objects");
		addObjectsButton.setDisable(true);
		Button removeObjectsButton = new Button("Remove objects");
		removeObjectsButton.setDisable(true);
		
		Label fileListLabel = new Label("File list          Layer:");
		GridPane.setColumnSpan(fileListLabel, 3);
		pane.add(fileListLabel, 2, 2);
		
		layerListView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ObjectLayer> observable, ObjectLayer oldValue, ObjectLayer newValue) -> {
			if (newValue == null) {
				fileListLabel.setText("File list          Layer:");
				objectListView.setItems(null);
				addObjectsButton.setDisable(true);
				removeObjectsButton.setDisable(true);
				return;
			}
			if (!(newValue.objectFiles instanceof ObservableList)) {
				newValue.objectFiles = FXCollections.observableArrayList(newValue.objectFiles);
			}
			fileListLabel.setText("File list          Layer: " + newValue.getName());
			objectListView.setItems((ObservableList<File>) newValue.objectFiles);
			addObjectsButton.setDisable(false);
			removeObjectsButton.setDisable(false);
		});

		objectListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		objectListView.setPrefSize(600, 400);
		GridPane.setColumnSpan(objectListView, 3);
		pane.add(objectListView, 2, 3);
		
		addObjectsButton.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			ArrayList<String> allExtensions = new ArrayList<String>();
			for (CustomObjectFormat format: CustomObjectFormatRegistry.getFormats()) {
				allExtensions.add("*." + format.formatSuffix());
			}
			fileChooser.getExtensionFilters().add(new ExtensionFilter("All custom objects", allExtensions));
			for (CustomObjectFormat format: CustomObjectFormatRegistry.getFormats()) {
				fileChooser.getExtensionFilters().add(new ExtensionFilter(format.formatName(), "*." + format.formatSuffix()));
			}
			
			List<File> selectedFiles = fileChooser.showOpenMultipleDialog(addObjectsButton.getScene().getWindow());
			if (selectedFiles != null) {
				layerListView.getSelectionModel().getSelectedItem().objectFiles.addAll(selectedFiles);
			}
		});
		
		removeObjectsButton.setOnAction(e -> {
			layerListView.getSelectionModel().getSelectedItem().objectFiles.removeAll(objectListView.getSelectionModel().getSelectedItems());
			
		});
		
		pane.add(addObjectsButton, 2, 4);
		pane.add(removeObjectsButton, 3, 4);
		
		//----------------
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			layerManager.applyEditorLayers(layerListView.getItems());
			parameterPlaceAir.applyUiValue();
			parameterPlacementRadius.applyUiValue();
			reregisterIO();
		};
		
		return applyHandler;
	}

	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode node = objectMapper.createObjectNode();
		
		ArrayNode layersNode = objectMapper.createArrayNode();
		for (ObjectLayer layer: layerManager.getLayers()) {
			layersNode.add(layer.toJson());
		}
		node.set("layers", layersNode);
		
		node.put(placementRadius.getName(), placementRadius.getValue());
		node.put(placeAir.getName(), placeAir.getValue());
		
		return node;
	}

	@Override
	public void fromJson(JsonNode node) {
		layerManager.clearLayers();
		for (JsonNode layerNode: node.get("layers")) {
			layerManager.addLayer(new ObjectLayer(layerNode));
		}
		placementRadius.setValue(node.get(placementRadius.getName()).asInt());
		placeAir.setValue(node.get(placeAir.getName()).asBoolean());
		
		reregisterIO();
	}
	
	private class ObjectLayer {
		
		private String name;
		private boolean active = true;
		
		private List<File> objectFiles = new ArrayList<File>();
		private List<CustomObject> cachedObjects = null;
		
		public ObjectLayer(String name) {
			this.name = name;
		}
		
		public ObjectLayer(JsonNode node) {
			fromJson(node);
		}
		
		public String getName() {
			return name;
		}
		
		public boolean isActive() {
			return active;
		}
		
		public boolean hasInternalObjects() {
			return objectFiles.size() > 0;
		}
		
		public List<CustomObject> getCachedObjects() {
			if (cachedObjects == null && objectFiles.size() > 0) {
				cachedObjects = readObjectsFromFile(objectFiles);
			}
			return cachedObjects;
		}
		
		private List<CustomObject> readObjectsFromFile(List<File> objectFiles) {
			ArrayList<CustomObject> objects = new ArrayList<CustomObject>();
			
			for (File objectFile: objectFiles) {
				try {
					objects.add(CustomObjectFormatRegistry.getFormatForFile(objectFile).readObjectFromFile(objectFile));
				} catch (IOException ex) {
					ex.printStackTrace();
					return null;
				}
			}
			
			return objects;
		}
		
		public JsonNode toJson() {
			ObjectMapper objectMapper = new ObjectMapper();
			ObjectNode node = objectMapper.createObjectNode();
			
			node.put("name", name);
			node.put("active", isActive());
			
			ArrayNode filesNode = objectMapper.createArrayNode();
			if (objectFiles != null && !objectFiles.isEmpty()) {
				for (File f: objectFiles) {
					filesNode.add(f.getAbsolutePath());
				}
			}
			node.set("files", filesNode);
			
			return node;
		}
		
		public void fromJson(JsonNode node) {
			name = node.get("name").asText();
			active = node.get("active").asBoolean();
			
			objectFiles = new ArrayList<File>();
			JsonNode filePaths = node.get("files");
			for (JsonNode e: filePaths) {
				objectFiles.add(new File(e.asText()));
			}
			cachedObjects = null;
		}
		
		@Override
		protected ObjectLayer clone() throws CloneNotSupportedException {
			ObjectLayer clone = new ObjectLayer(name);
			if (objectFiles != null) {
				for (File f: objectFiles) {
					clone.objectFiles.add(new File(f, ""));
				}
			}
			return clone;
		}
	}
	
	private class LayerManager {
		private List<ObjectLayer> layers = new ArrayList<ObjectLayer>();
		
		public LayerManager() {
			layers.add(new ObjectLayer("Default"));
		}
		
		public void addLayer(ObjectLayer layer) {
			layers.add(layer);
		}
		
		public List<ObjectLayer> getLayers() {
			return layers;
		}
		
		public void clearLayers() {
			layers.clear();
		}
		
		public List<ObjectLayer> getEditorLayers() {
			List<ObjectLayer> editorLayers = new ArrayList<ObjectLayer>();
			try {
				for (ObjectLayer layer: layers) {
					editorLayers.add(layer.clone());
				}
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return editorLayers;
		}
		
		public void applyEditorLayers(List<ObjectLayer> layers) {
			List<ObjectLayer> applyLayers = new ArrayList<ObjectLayer>();
			try {
				for (ObjectLayer layer: layers) {
					applyLayers.add(layer.clone());
				}
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			
			this.layers = applyLayers;
		}
	}
	
	private class LayerCell extends ListCell<ObjectLayer> {
		private ObjectLayer layer;
		
		private final HBox hbox;
		private final TextField nameField = new TextField("");
		private final Button editButton = new Button("Edit");
		private final MenuItem deleteLayerItem = new MenuItem("Delete");
		
		public LayerCell(ListView<ObjectLayer> parrentListView) {
			HBox.setHgrow(nameField, Priority.ALWAYS);
			nameField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
				if (newValue.length() > 0) {
					layer.name = newValue;
				}
			});
			
			deleteLayerItem.setOnAction(e -> {
				parrentListView.getItems().remove(layer);
			});
			editButton.setContextMenu(new ContextMenu(deleteLayerItem));
			
			editButton.setOnAction(e -> {
				getOnEdit().handle(e);
			});
			
			hbox = new HBox(nameField, editButton);
		}

		/**
		 * The edit action, which is invoked whenever the edit button is fired. This may
		 * be due to the user clicking on the edit button with the mouse, or by a touch
		 * event, or by a key press, or if the developer programmatically invokes the
		 * {@link #fire()} method.
		 */
		public final ObjectProperty<EventHandler<ActionEvent>> onEditProperty() {
			return onEdit;
		}

		public final void setOnEdit(EventHandler<ActionEvent> value) {
			onEditProperty().set(value);
		}

		public final EventHandler<ActionEvent> getOnEdit() {
			return onEditProperty().get();
		}

		private ObjectProperty<EventHandler<ActionEvent>> onEdit = new SimpleObjectProperty<EventHandler<ActionEvent>>();

		@Override
		protected void updateItem(ObjectLayer item, boolean empty) {
			super.updateItem(item, empty);
			setText(null);
			setGraphic(null);
			
			if (item != null) {
				layer = item;
				nameField.setText(layer.name);
				setGraphic(hbox);
			}
		}
	}
}
