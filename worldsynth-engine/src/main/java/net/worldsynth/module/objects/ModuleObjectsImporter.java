/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.customobject.CustomObjectFormatRegistry;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;

public class ModuleObjectsImporter extends AbstractModule {
	
	private List<File> objectFiles = new ArrayList<File>();
	private List<CustomObject> cachedObjects;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeObjects requestData = (DatatypeObjects) request.data;
		
		if (objectFiles == null) {
			return null;
		}
		else if (objectFiles.isEmpty()) {
			return null;
		}
		
		//Check if the object files are cached, if not read them from file to cache
		if (cachedObjects == null) {
			cachedObjects = new ArrayList<CustomObject>();
			for (File objectFile: objectFiles) {
				try {
					cachedObjects.add(CustomObjectFormatRegistry.getFormatForFile(objectFile).readObjectFromFile(objectFile));
				} catch (IOException e) {
					e.printStackTrace();
					cachedObjects = null;
					return null;
				}
			}
		}
		
		//Clone objects for return value based on seed
		for (LocatedCustomObject locatedObject: requestData.getLocatedObjects()) {
			locatedObject.setObject(cachedObjects.get((int) (locatedObject.getSeed() % cachedObjects.size())));
		}
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Object importer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeObjects(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ListView<File> objectListView = new ListView<>(FXCollections.observableArrayList(objectFiles));
		objectListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		objectListView.setPrefSize(600, 400);
		
		GridPane.setColumnSpan(objectListView, 3);
		pane.add(objectListView, 0, 0);
		
		Button addObjectsButton = new Button("Add");
		Button removeObjectsButton = new Button("Remove");
		
		addObjectsButton.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			ArrayList<String> allExtensions = new ArrayList<String>();
			for (CustomObjectFormat format: CustomObjectFormatRegistry.getFormats()) {
				allExtensions.add("*." + format.formatSuffix());
			}
			fileChooser.getExtensionFilters().add(new ExtensionFilter("All custom objects", allExtensions));
			for (CustomObjectFormat format: CustomObjectFormatRegistry.getFormats()) {
				fileChooser.getExtensionFilters().add(new ExtensionFilter(format.formatName(), "*." + format.formatSuffix()));
			}
			
			List<File> selectedFiles = fileChooser.showOpenMultipleDialog(addObjectsButton.getScene().getWindow());
			if (selectedFiles != null) {
				objectListView.getItems().addAll(selectedFiles);
			}
		});
		
		removeObjectsButton.setOnAction(e -> {
			objectListView.getItems().removeAll(objectListView.getSelectionModel().getSelectedItems());
		});
		
		pane.add(addObjectsButton, 0, 1);
		pane.add(removeObjectsButton, 1, 1);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			objectFiles = objectListView.getItems();
			cachedObjects = null;
		};
		
		return applyHandler;
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode node = objectMapper.createObjectNode();
		
		ArrayNode filesNode = objectMapper.createArrayNode();
		for (File f: objectFiles) {
			filesNode.add(f.getAbsolutePath());
		}
		node.set("files", filesNode);
		
		return node;
	}

	@Override
	public void fromJson(JsonNode node) {
		objectFiles = new ArrayList<File>();
		for (JsonNode s: node.get("files")) {
			objectFiles.add(new File(s.asText()));
		}
		cachedObjects = null;
	}
}
