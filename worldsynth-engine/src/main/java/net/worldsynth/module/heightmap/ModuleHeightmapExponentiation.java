/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleHeightmapExponentiation extends AbstractModule {
	
	private DoubleParameter exponent = new DoubleParameter("exponent", "Exponent", null, 2.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1.0, 16.0);
	private BooleanParameter normalized = new BooleanParameter("normalized", "Normalized", null, true);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				exponent,
				normalized
		};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int spw = requestData.mapPointsWidth;
		int spl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		double exponent = this.exponent.getValue();
		boolean normalized = this.normalized.getValue();
		
		//Read in input
		if (inputs.get("input") == null) {
			// If the input is null, there is no input and then just return null
			return null;
		}
		DatatypeHeightmap input = ((DatatypeHeightmap) inputs.get("input"));
		
		//----------BUILD----------//
		
		float[][] heightmap = new float[spw][spl];
		
		// Apply normalization
		final Function<Float, Float> f;
		if (normalized) {
			f = t -> (float) Math.pow(t / normalizedHeight, exponent) * normalizedHeight;
		}
		else {
			f = t -> (float) Math.pow(t, exponent);
		}
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < spl; v++) {
				heightmap[u][v] = f.apply(input.getLocalHeight(u, v));
			}
		}
		
		requestData.setHeightmap(heightmap);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Exponentiation";
	}
	
	@Override
	public String getModuleMetaTag() {
		DecimalFormat df = new DecimalFormat("#.###");
		df.setRoundingMode(RoundingMode.HALF_UP);
		return "^" + df.format(exponent.getValue());
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
