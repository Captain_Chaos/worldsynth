/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.WorldExtentParameter;
import net.worldsynth.util.HeightmapUtil;
import net.worldsynth.util.math.MathHelperScalar;

public class ModuleHeightmapExtentGradient extends AbstractModule {
	
	private WorldExtentParameter gradientExtent = new WorldExtentParameter("extent", "Extent", null, this);
	private DoubleParameter gradientScale = new DoubleParameter("scale", "Scale", null, 100.0, 0.0, Double.MAX_VALUE, 0.0, 1000.0);
	private EnumParameter<GradientShape> gradientShape = new EnumParameter<GradientShape>("shape", "Gradient shape", null, GradientShape.class, GradientShape.ELLIPTIC);
	private EnumParameter<GradientStyle> gradientStyle = new EnumParameter<ModuleHeightmapExtentGradient.GradientStyle>("style", "Gradient style", null, GradientStyle.class, GradientStyle.SCURVE);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
//		gradientExtent = new WorldExtentParameter("extent", "Extent", null, this);
		
		AbstractParameter<?>[] p = {
				gradientExtent,
				gradientScale,
				gradientShape,
				gradientStyle
				};
		return p;
	}
	
	@Override
	protected void postInit() {
//		gradientExtent = getNewExtentParameter();
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		WorldExtent extent = gradientExtent.getValue();
		if (extent == null) {
			return null;
		}
		GradientShape shape = this.gradientShape.getValue();
		GradientStyle style = this.gradientStyle.getValue();
		
		//Read in scale
		double scaleValue = this.gradientScale.getValue();
		if (inputs.get("scale") != null) {
			scaleValue = ((DatatypeScalar) inputs.get("scale")).getValue();
		}
		
		//Read in mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = getHeightAt(x+u*res, z+v*res, scaleValue, extent, shape, style) * normalizedHeight;
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(map, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	private float getHeightAt(double x, double z, double scale, WorldExtent extent, GradientShape shape, GradientStyle style) {
		float h = 0.0f;
		
		if (shape == GradientShape.RECTANGULAR) {
			h = getRectangularGradient(x, z, scale, extent);
		}
		else if (shape == GradientShape.ELLIPTIC) {
			h = getElipticalGradient(x, z, scale, extent);
		}
		
		h = Math.min(h, 1.0f);
		h = Math.max(h, 0.0f);
		
		switch (style) {
		case LINEAR:
			break;
		case SCURVE:
			h = h * h * h * (h * (h * 6 - 15) + 10);
			break;
		case DOME:
			h = 1.0f - h;
			h = (float) Math.sqrt(1.0f - h * h);
			break;
		case SPIKE:
			h = 1.0f - (float) Math.sqrt(1.0f - h * h);
			break;
		}
		
		h = Math.min(h, 1.0f);
		h = Math.max(h, 0.0f);
		
		return h;
	}
	
	private float getRectangularGradient(double x, double z, double scale, WorldExtent extent) {
		if (extent.containsCoordinate(x, z)) {
			double west = x - extent.getX();
			double east = (extent.getX()+extent.getWidth()) - x;
			double north = z - extent.getZ();
			double south = (extent.getZ()+extent.getLength()) - z;
			
			double min = Math.min(Math.min(west, east), Math.min(north, south));
			return (float) (MathHelperScalar.clamp(min / scale, 0.0, 1.0));
		}
		else {
			return 0.0f;
		}
	}
	
	private float getElipticalGradient(double x, double z, double scale, WorldExtent extent) {
		if (extent.containsCoordinate(x, z)) {
			double cx = extent.getX() + extent.getWidth() / 2.0;
			double cz = extent.getZ() + extent.getLength() / 2.0;
			
			double dx = (x - cx) / (extent.getWidth() / 2.0);
			double dz = (z - cz) / (extent.getLength() / 2.0);
			
			double dist = 1.0 - Math.sqrt(dx * dx + dz * dz);
			dist *= (Math.max(extent.getWidth(), extent.getLength()) / 2.0) / scale;
			
			return (float) dist;
		}
		else {
			return 0.0f;
		}
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		inputRequests.put("scale", new ModuleInputRequest(getInput(0), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput(1), (DatatypeHeightmap) outputRequest.data));
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Extent gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum GradientShape {
		RECTANGULAR, ELLIPTIC;
	}
	
	private enum GradientStyle {
		LINEAR, SCURVE, DOME, SPIKE;
	}
}
