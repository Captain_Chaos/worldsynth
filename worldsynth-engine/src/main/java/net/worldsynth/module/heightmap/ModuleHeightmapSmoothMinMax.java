/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.util.HeightmapUtil;

public class ModuleHeightmapSmoothMinMax extends AbstractModule {
	
	private DoubleParameter factor = new DoubleParameter("factor", "Factor",
			"Negative values give smooth min, positive values give smooth max.",
			0.1, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1.0, 1.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				factor
		};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		double factor = this.factor.getValue();
		
		//Check if both inputs are available
		if (inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		//Read in the primary and secondary heightmap
		float[][] primaryMap = ((DatatypeHeightmap) inputs.get("primary")).getHeightmap();
		float[][] secondaryMap = ((DatatypeHeightmap) inputs.get("secondary")).getHeightmap();
		
		
		//Read mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
				
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				double i0 = primaryMap[u][v];
				double i1 = secondaryMap[u][v];
				
				//Smooth max
				map[u][v] = (float) ((i0 * Math.exp(factor * i0) + i1 * Math.exp(factor * i1)) / (Math.exp(factor * i0) + Math.exp(factor * i1)));
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(map, primaryMap, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput("Primary"), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput("Secondary"), outputRequest.data));
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Smooth min/max";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary"),
				new ModuleInput(new DatatypeHeightmap(), "Secondary"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

}
