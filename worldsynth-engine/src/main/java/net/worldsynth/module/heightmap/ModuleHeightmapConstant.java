/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;
import net.worldsynth.util.HeightmapUtil;

public class ModuleHeightmapConstant extends AbstractModule {
	
	private FloatParameter constant = new FloatParameter("constant", "Constant", null, 0.0f, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, 0.0f, 256.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				constant
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		inputRequests.put("heightinput", new ModuleInputRequest(getInput(0), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput(1), (DatatypeHeightmap) outputRequest.data));
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//

		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		//Read in constant
		float contantValue = this.constant.getValue();
		if (inputs.get("heightinput") != null) {
			contantValue = (float) ((DatatypeScalar) inputs.get("heightinput")).getValue();
		}
		
		//Read in mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = contantValue;
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(map, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Constant";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Height"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
