/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.util.HeightmapUtil;
import net.worldsynth.util.gen.Permutation;

public class ModuleHeightmapSimplePerlin extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 100.0, 1.0, Double.POSITIVE_INFINITY, 1.0, 1000.0);
	private DoubleParameter amplitude = new DoubleParameter("amplitude", "Amplitude", null, 128.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 512.0);
	private DoubleParameter offset = new DoubleParameter("offset", "Offset", null, 128.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -256.0, 256.0);
	private DoubleParameter distortion = new DoubleParameter("distortion", "Distortion", null, 1.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 100.0);
	private EnumParameter<Shape> shape = new EnumParameter<Shape>("shape", "Shape", null, Shape.class, Shape.STANDARD);
	
	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, permutationSize, 1);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				amplitude,
				offset,
				shape,
				seed,
				distortion
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		double scale = this.scale.getValue();
		double amplitude = this.amplitude.getValue();
		double offset = this.offset.getValue();
		double distortion = this.distortion.getValue();
		Shape shape = this.shape.getValue();
		float[][] amplitudeMap = null;
		float[][] offsetMap = null;
		float[][][] distortionMap = null;
		float[][] mask = null;
		
		//Read in scale
		
		if (inputs.get("scale") != null) {
			scale = ((DatatypeScalar) inputs.get("scale")).getValue();
		}
		
		//Read in amplitude
		if (inputs.get("amplitude") != null) {
			if (inputs.get("amplitude") instanceof DatatypeScalar) {
				amplitude = ((DatatypeScalar) inputs.get("amplitude")).getValue();
			}
			else {
				amplitudeMap = ((DatatypeHeightmap) inputs.get("amplitude")).getHeightmap();
			}
		}
		
		//Read in offset
		if (inputs.get("offset") != null) {
			if (inputs.get("offset") instanceof DatatypeScalar) {
				offset = ((DatatypeScalar) inputs.get("offset")).getValue();
			}
			else {
				offsetMap = ((DatatypeHeightmap)inputs.get("offset")).getHeightmap();
			}
		}
		
		//Read in distortion
		if (inputs.get("distortion") != null) {
			if (inputs.get("distortion") instanceof DatatypeVectormap) {
				distortionMap = ((DatatypeVectormap) inputs.get("distortion")).getVectormap();
			}
			else {
				float[][] distortionHeightmap = ((DatatypeHeightmap) inputs.get("distortion")).getHeightmap();
				distortionMap = new float[mpw][mpl][2];
				for (int u = 0; u < mpw; u++) {
					for (int v = 0; v < mpl; v++) {
						distortionMap[u][v][0] = (float) Math.cos(distortionHeightmap[u][v]/normalizedHeight * Math.PI * 2.0);
						distortionMap[u][v][1] = (float) Math.sin(distortionHeightmap[u][v]/normalizedHeight * Math.PI * 2.0);
					}
				}
			}
		}
		
		//Read in mask
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		//Has some input maps
		if (amplitudeMap != null || offsetMap != null || distortionMap != null) {
			double xDistortion = 0.0;
			double zDistortion = 0.0;
			double amplitudeLocal = amplitude;
			double offsetLocal = offset;
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					if (amplitudeMap != null) amplitudeLocal = amplitudeMap[u][v]/normalizedHeight * amplitude;
					if (offsetMap != null) offsetLocal = offsetMap[u][v] + offset;
					if (distortionMap != null) {
						xDistortion = distortionMap[u][v][0] * distortion;
						zDistortion = distortionMap[u][v][1] * distortion;
					}
					
					map[u][v] = (float) getHeightAt(x+u*res+xDistortion, z+v*res+zDistortion, scale, amplitudeLocal, offsetLocal, shape);
				}
			}
		}
		//Has only values and no map
		else {
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, amplitude, offset, shape);
				}
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(map, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("scale", new ModuleInputRequest(getInput("Scale"), new DatatypeScalar()));
		inputRequests.put("amplitude", new ModuleInputRequest(getInput("Amplitude"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput("Offset"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		DatatypeVectormap requestVectorMap = new DatatypeVectormap(ord.extent, ord.resolution);
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), new DatatypeMultitype(new AbstractDatatype[] {(DatatypeHeightmap)outputRequest.data, requestVectorMap})));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), (DatatypeHeightmap)outputRequest.data));
		
		return inputRequests;
	}
	
	public double getHeightAt(double x, double y, double scale, double amplitude, double offset, Shape shape) {
		double height = perlin(x/scale, y/scale, shape);
		height *= amplitude;
		height += offset;
		return height;
	}
	
	private double perlin(double x, double y, Shape shape) {
		
		if (repeat > 0) {
			if (x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if (y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		
		double u = easeCurve(xf);
		double v = easeCurve(yf);
		
		int aa, ab, ba, bb;
		aa = permutation.lHash(0, xi     , yi     );
		ab = permutation.lHash(0, xi     , inc(yi));
		ba = permutation.lHash(0, inc(xi), yi     );
		bb = permutation.lHash(0, inc(xi), inc(yi));
		
		double a1, a2, a3, a4;
		a1 = grad(aa, xf  , yf  , 0);
		a2 = grad(ba, xf-1, yf, 0);
		a3 = grad(ab, xf, yf-1, 0);
		a4 = grad(bb, xf-1, yf-1, 0);
		
		double a12, a34;
		a12 = lerp(a2, a1, u);
		a34 = lerp(a4, a3, u);
		double height = lerp(a34, a12, v);
		
		switch (shape) {
		case STANDARD:
			break;
		case RIDGED:
			height = 1.0f - Math.abs(height);
			break;
		case BOWLY:
			height = Math.abs(height);
			break;
		}
		
		return height;
	}
	
	private int inc(int num) {
		num++;
		int ret;
		if (num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}
	
	private double easeCurve(double t) {
		return t * t * t * (t * (t * 6 - 15) + 10);
	}
	
	private double grad(int hash, double x, double y, double z) {
		switch (hash & 0xF)
	    {
	        case 0x0: return  x + y;
	        case 0x1: return -x + y;
	        case 0x2: return  x - y;
	        case 0x3: return -x - y;
	        case 0x4: return  x + z;
	        case 0x5: return -x + z;
	        case 0x6: return  x - z;
	        case 0x7: return -x - z;
	        case 0x8: return  y + z;
	        case 0x9: return -y + z;
	        case 0xA: return  y - z;
	        case 0xB: return -y - z;
	        case 0xC: return  y + x;
	        case 0xD: return -y + z;
	        case 0xE: return  y - x;
	        case 0xF: return -y - z;
	        default: return 0; // never happens
	    }
	}
	
	private double lerp(double a, double b, double x) {
	    return a*x + b*(1-x);
	}

	@Override
	public String getModuleName() {
		return "Simple perlin";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Amplitude"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Offset"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeHeightmap(), new DatatypeVectormap()}), "Distortion"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum Shape {
		STANDARD, RIDGED, BOWLY;
	}
}
