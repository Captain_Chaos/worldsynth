/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleVectorfieldConstant extends AbstractModule {
	
	private FloatParameter xComponent = new FloatParameter("xcomponent", "X component", null, 0.0f, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, -1.0f, 1.0f);
	private FloatParameter zComponent = new FloatParameter("zcomponent", "Z component", null, 0.0f, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, -1.0f, 1.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				xComponent,
				zComponent
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//----------BUILD----------//
		
		float[][][] field = new float[mpw][mpl][2];
		
		float[] vec = {xComponent.getValue(), zComponent.getValue()};
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				field[u][v] = vec;
			}
		}
		
		requestData.setVectormap(field);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Vectormap constant";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeVectormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
