/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleVectorfieldHeightmapGradient extends AbstractModule {
	
	private FloatParameter gain = new FloatParameter("gain", "Gain", null, 1.0f, 0.0f, Float.MAX_VALUE, 0.0f, 1.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				gain
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		float res = (float) requestData.resolution;
		
		//----------READ INPUTS----------//
		
		float gain = this.gain.getValue();
		
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//----------BUILD----------//
		
		float[][][] field = new float[mpw][mpl][2];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float[] gradient = getGradient(inputMap, u+1, v+1, res);
				
				gradient[0] *= gain;
				gradient[1] *= gain;
				
				field[u][v] = gradient;
			}
		}
		
		requestData.setVectormap(field);
		
		return requestData;
	}
	
	private float[] getGradient(float[][] heightmap, int x, int y, float res) {
		
		float cph = getHeight(heightmap, x, y);
		float dnx = cph - getHeight(heightmap, x-1, y  );
		float dpx = cph - getHeight(heightmap, x+1, y  );
		float dny = cph - getHeight(heightmap, x  , y-1);
		float dpy = cph - getHeight(heightmap, x  , y+1);
		
		float dx = (dnx - dpx) / (2*res);
		float dy = (dny - dpy) / (2*res);
		
		float[] gradient = {dx, dy};
		
		return gradient;
	}
	
	private float getHeight(float[][] heightmap, int x, int y) {
		if (x < 0) {
			x = 0;
		}
		else if (x >= heightmap.length) {
			x = heightmap.length-1;
		}
		
		if (y < 0) {
			y = 0;
		}
		else if (y >= heightmap[0].length) {
			y = heightmap[0].length-1;
		}
		
		float height = heightmap[(int)x][(int)y];
		
		return height;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeVectormap ord = (DatatypeVectormap) outputRequest.data;
		double res = ord.resolution;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(BuildExtent.expandedBuildExtent(ord.extent, res, 0, res), res);
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Vectormap gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeVectormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
