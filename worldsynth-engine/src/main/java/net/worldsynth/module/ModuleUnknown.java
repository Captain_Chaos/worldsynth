/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter.Indenter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeNull;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;

public class ModuleUnknown extends AbstractModule {
	
	private ArrayList<String> inputConnections = new ArrayList<String>();
	private ArrayList<String> outputConnections = new ArrayList<String>();
	
	String moduleclass;
	private String moduleclassName;
	private JsonNode moduleJsonNode;
	
	public ModuleUnknown(String moduleclass, JsonNode moduleJsonNode) {
		this.moduleclass = moduleclass;
		this.moduleJsonNode = moduleJsonNode;
		
		String[] s = moduleclass.split("\\.");
		moduleclassName = s[s.length-1];
	}
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return null;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Unknown - " + moduleclassName;
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.UNKNOWN;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] in = new ModuleInput[inputConnections.size()];
		for (int i = 0; i < inputConnections.size(); i++) {
			in[i] = new ModuleInput(new DatatypeNull(), inputConnections.get(i));
		}
		return in;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] out = new ModuleOutput[outputConnections.size()];
		for (int i = 0; i < outputConnections.size(); i++) {
			out[i] = new ModuleOutput(new DatatypeNull(), outputConnections.get(i));
		}
		return out;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		Label label = new Label("This is an unknown module.\n"
				+ "The module might be unavailable because it is part of a missing library, "
				+ "or the module might be removed from newer versions of a third party libray "
				+ "or the WorldSynth standard library."
				+ "\n\nModule ID:\n" + moduleclass + "\n\n");
		label.setWrapText(true);
		label.setPrefWidth(600);
		
		String parameters;
		try {
			DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
			Indenter indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
			printer.indentObjectsWith(indenter);
			printer.indentArraysWith(indenter);
			
			ObjectMapper objectMapper = new ObjectMapper();
			parameters = objectMapper.writer(printer).writeValueAsString(moduleJsonNode);
		} catch (JsonProcessingException e) {
			parameters = e.getStackTrace().toString();
		}
		TextArea textArea = new TextArea(parameters);
		textArea.setEditable(false);
		ScrollPane scrollPane = new ScrollPane(textArea);
		scrollPane.setFitToWidth(true);
		
		VBox vBox = new VBox(label, scrollPane);
		
		pane.add(vBox, 0, 0);
		return null;
	}

	@Override
	public JsonNode toJson() {
		return moduleJsonNode;
	}

	@Override
	public void fromJson(JsonNode node) {
		moduleJsonNode = node;
	}
	
	public void addInput(String inputName) {
		inputConnections.add(inputName);
		reregisterIO();
	}
	
	public void addOutput(String outputName) {
		outputConnections.add(outputName);
		reregisterIO();
	}
}
