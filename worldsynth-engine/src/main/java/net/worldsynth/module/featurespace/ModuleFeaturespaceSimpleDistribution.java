/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.featurespace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturespace;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.featurepoint.Featurepoint3D;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.util.gen.Permutation;

public class ModuleFeaturespaceSimpleDistribution extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 100.0, 1.0, Double.MAX_VALUE, 1.0, 200.0);
	private DoubleParameter probability = new DoubleParameter("probability", "Probability", null, 0.2, 0.0, 1.0);
	private DoubleParameter displacement = new DoubleParameter("displacement", "Displacement", null, 0.0, 0.0, 1.0);
	
	private final int permutationSize = 1024;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, permutationSize, 5);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				probability,
				displacement,
				seed
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeFeaturespace ord = (DatatypeFeaturespace) outputRequest.data;
		
		DatatypeValuespace valuespaceRequestData = new DatatypeValuespace(ord.extent, ord.resolution);
		inputRequests.put("probabilityspace", new ModuleInputRequest(getInput(0), valuespaceRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeaturespace requestData = (DatatypeFeaturespace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double width = requestData.extent.getWidth();
		double height = requestData.extent.getHeight();
		double length = requestData.extent.getLength();
		double resolution = requestData.resolution;
		
		//----------READ INPUTS----------//
		
		float[][][] probabilitySpace = null;
		if (inputs.get("probabilityspace") != null) {
			probabilitySpace = ((DatatypeValuespace) inputs.get("probabilityspace")).getValuespace();
		}
		
		//----------BUILD----------//
		
		double scale = this.scale.getValue();
		double displacement = this.displacement.getValue();
		double probability = this.probability.getValue();
		
		int minXPoint = (int) Math.floor(x/scale);
		int maxXPoint = (int) Math.ceil((x+width)/scale);
		int minYPoint = (int) Math.floor(y/scale);
		int maxYPoint = (int) Math.ceil((y+height)/scale);
		int minZPoint = (int) Math.floor(z/scale);
		int maxZPoint = (int) Math.ceil((z+length)/scale);
		
		ArrayList<Featurepoint3D> generatedPoints = new ArrayList<Featurepoint3D>();
		
		for (int u = minXPoint; u < maxXPoint; u++) {
			for (int v = minYPoint; v < maxYPoint; v++) {
				for (int w = minZPoint; w < maxZPoint; w++) {
					int[] hashCoordinates = hashCoordianteAt(u, v, w);
					
					double xPos = u * scale;
					double yPos = v * scale;
					double zPos = w * scale;
					
					double xDisplace = permutation.lUnitHash(0, hashCoordinates[0], hashCoordinates[1], hashCoordinates[2]);
					double yDisplace = permutation.lUnitHash(1, hashCoordinates[0], hashCoordinates[1], hashCoordinates[2]);
					double zDisplace = permutation.lUnitHash(2, hashCoordinates[0], hashCoordinates[1], hashCoordinates[2]);
					
					xPos += -scale*displacement/2.0f + xDisplace*scale*displacement;
					yPos += -scale*displacement/2.0f + yDisplace*scale*displacement;
					zPos += -scale*displacement/2.0f + zDisplace*scale*displacement;
					
					if (insideBoundary(xPos, yPos, zPos, requestData)) {
						if (probabilitySpace != null) {
							probability = probabilitySpace[(int) ((xPos-x)/resolution)][(int) ((yPos-y)/resolution)][(int) ((zPos-z)/resolution)];
						}
						if (permutation.lUnitHash(3, hashCoordinates[0], hashCoordinates[1], hashCoordinates[2]) < probability) {
							generatedPoints.add(new Featurepoint3D(xPos, yPos, zPos, permutation.lHash(4, hashCoordinates[0], hashCoordinates[1], hashCoordinates[2])));
						}
					}
				}
			}
		}
		
		Featurepoint3D[] points = new Featurepoint3D[generatedPoints.size()];
		generatedPoints.toArray(points);
		requestData.setFeaturepoints(points);
		
		return requestData;
	}
	
	private boolean insideBoundary(double x, double y, double z, DatatypeFeaturespace requestData) {
		return requestData.extent.isContained(x, y, z);
	}
	
	private int[] hashCoordianteAt(int x, int y, int z) {
		if (repeat > 0) {
			x = x < 0 ? repeat+(x%repeat) : x%repeat;
			y = y < 0 ? repeat+(y%repeat) : y%repeat;
			z = z < 0 ? repeat+(z%repeat) : z%repeat;
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		return new int[] {x & 255, y & 255, z & 255};
	}

	@Override
	public String getModuleName() {
		return "Simple distribution";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Probability")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeaturespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
