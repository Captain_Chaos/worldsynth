/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.color.ColorGradient;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.fx.ColorGradientEditor;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;

public class ModuleColormapGradient extends AbstractModule {
	
	//TODO Port parameter to new system
	private ColorGradient gradient = new ColorGradient(new float[][] {{0.0f, 0.0f}, {1.0f, 1.0f}});
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		float[][][] map = new float[mpw][mpl][3];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				
				float[] cc = gradient.getColorComponents(inputMap[u][v]/normalizedHeight);
				
				map[u][v][0] = cc[0];
				map[u][v][1] = cc[1];
				map[u][v][2] = cc[2];
			}
		}
		
		requestData.setColormap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap ord = (DatatypeColormap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Constant //////////
		ColorGradientEditor gradientEditor = new ColorGradientEditor(gradient);
		pane.add(gradientEditor, 0, 0);
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			gradient = gradientEditor.getGradient();
		};
		
		return applyHandler;
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode objectNode = objectMapper.createObjectNode();
		
		objectNode.set("gradient", gradient.toJson());
		
		return objectNode;
	}

	@Override
	public void fromJson(JsonNode node) {
		gradient = new ColorGradient(node.get("gradient"));
	}
}
