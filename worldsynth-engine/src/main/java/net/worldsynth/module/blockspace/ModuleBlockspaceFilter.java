/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.list.MaterialStateListParameter;

public class ModuleBlockspaceFilter extends AbstractModule {
	
	private BooleanParameter inclusive = new BooleanParameter("inclusive", "Inclusive", null, true);
	private MaterialStateListParameter palette = new MaterialStateListParameter("palette", "Material palette", null, Arrays.asList(MaterialRegistry.getDefaultMaterial().getDefaultState()));
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				inclusive,
				palette
		};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		boolean inclusive = this.inclusive.getValue();
		HashSet<MaterialState<?, ?>> palette = new HashSet<MaterialState<?, ?>>(this.palette.getValueList());
		
		if (inputs.get("input") == null) {
			return null;
		}
		MaterialState<?, ?>[][][] blockspace = ((DatatypeBlockspace) inputs.get("input")).getBlockspace();
		
		//----------BUILD----------//
		
		MaterialState<?, ?> air = MaterialRegistry.getDefaultAir().getDefaultState();
		
		//Apply cover on top of existing blocks
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					boolean inPalette = palette.contains(blockspace[u][v][w]);
					if (inPalette && !inclusive) {
						blockspace[u][v][w] = air;
					}
					else if (!inPalette && inclusive) {
						blockspace[u][v][w] = air;
					}
				}
			}
		}
		
		requestData.setBlockspace(blockspace);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace outputRequestData = (DatatypeBlockspace) outputRequest.data;
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Filter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
