/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.*;
import net.worldsynth.util.Axis;
import net.worldsynth.util.Tuple;
import net.worldsynth.util.gen.Permutation;

public class ModuleBlockspaceCellularize extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleTupleParameter scale = new DoubleTupleParameter("scale", "Scale", null, 0.0, Double.POSITIVE_INFINITY, 32.0, 32.0, 32.0);
	private EnumParameter<CellDistribution> cellDistribution = new EnumParameter<CellDistribution>("distribution", "Cell distribution", null, CellDistribution.class, CellDistribution.VORONOI);
	private EnumParameter<DistanceFunction> distanceFunction = new EnumParameter<DistanceFunction>("distancefunction", "Distance function", null, DistanceFunction.class, DistanceFunction.EUCLIDEAN);
	private Axes3DParameter sampleAttraction = new Axes3DParameter("sampleattraction", "Sample attraction", null, true, true, true);

	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, permutationSize, 3);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				cellDistribution,
				distanceFunction,
				sampleAttraction,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		Tuple<Double> scale = this.scale.getValue();
		CellDistribution cellDistribution = this.cellDistribution.getValue();
		DistanceFunction distanceFunction = this.distanceFunction.getValue();
		boolean attractionX = this.sampleAttraction.getValue().get(Axis.X);
		boolean attractionY = this.sampleAttraction.getValue().get(Axis.Y);
		boolean attractionZ = this.sampleAttraction.getValue().get(Axis.Z);
		
		//Read in input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeBlockspace input = (DatatypeBlockspace) inputs.get("input");
		
		//----------BUILD----------//
		
		MaterialState<?, ?>[][][] materials = new MaterialState<?, ?>[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					materials[u][v][w] = getMaterialAt(x+u*res, y+v*res, z+w*res, scale, input, cellDistribution, distanceFunction, attractionX, attractionY, attractionZ);
				}
			}
		}
		
		requestData.setBlockspace(materials);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace)outputRequest.data;
		
		double expandX = Math.ceil(Math.abs(scale.getValue().getValue(0) * 2.0) / ord.resolution) * ord.resolution;
		double expandY = Math.ceil(Math.abs(scale.getValue().getValue(1) * 2.0) / ord.resolution) * ord.resolution;
		double expandZ = Math.ceil(Math.abs(scale.getValue().getValue(2) * 2.0) / ord.resolution) * ord.resolution;
		DatatypeBlockspace inputRequestDatatype = new DatatypeBlockspace(BuildExtent.expandedBuildExtent(ord.extent, expandX, expandY, expandZ), ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));
		
		return inputRequests;
	}
	
	public MaterialState<?, ?> getMaterialAt(double x, double y, double z, Tuple<Double> scale, DatatypeBlockspace blockspace, CellDistribution cellDistribution, DistanceFunction distanceFunction, boolean ax, boolean ay, boolean az) {
		Point3D p = closestPoint(x/scale.getValue(0), y/scale.getValue(1), z/scale.getValue(2), cellDistribution, distanceFunction);

		double sx = ax ? p.x * scale.getValue(0) : x;
		double sy = ay ? p.y * scale.getValue(1) : y;
		double sz = az ? p.z * scale.getValue(2) : z;
		return blockspace.getGlobalMaterial(sx, sy, sz);
	}
	
	private Point3D closestPoint(double x, double y, double z, CellDistribution cellDistribution, DistanceFunction distanceFunction) {
		double initx = x, inity = y, initz = z;
		
		if (repeat > 0) {
			if (x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if (y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
			if (z < 0) {
				z = repeat+(z%repeat);
			}
			else {
				z = z%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		int zi = (int)z & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		double zf = z - (int)z;
		
		//Calculate the local coordinates of the 9 closest featurepoints
		double[][][] fx = new double[3][3][3];
		double[][][] fy = new double[3][3][3];
		double[][][] fz = new double[3][3][3];
		
		double minDist = Double.POSITIVE_INFINITY;
		double minDistX = 0, minDistY = 0, minDistZ = 0;
		for (int ix = 0; ix < 3; ix++) {
			for (int iy = 0; iy < 3; iy++) {
				for (int iz = 0; iz < 3; iz++) {
					int cx = inc(xi, ix-1);
					int cy = inc(yi, iy-1);
					int cz = inc(zi, iz-1);
					
					int xh = permutation.lHash(0, cx, cy, cz);
					int yh = permutation.lHash(1, cx, cy, cz);
					int zh = permutation.lHash(2, cx, cy, cz);
					
					double xoffset = 0.5;
					double yoffset = 0.5;
					double zoffset = 0.5;
					
					if (cellDistribution == CellDistribution.VORONOI) {
						xoffset = (double) xh/(double) repeat;
						yoffset = (double) yh/(double) repeat;
						zoffset = (double) zh/(double) repeat;
					}
					
					fx[ix][iy][iz] = (double) (ix-1) + xoffset;
					fy[ix][iy][iz] = (double) (iy-1) + yoffset;
					fz[ix][iy][iz] = (double) (iz-1) + zoffset;
					
					double xdist = fx[ix][iy][iz] - xf;
					double ydist = fy[ix][iy][iz] - yf;
					double zdist = fz[ix][iy][iz] - zf;
					
					//Distance from point
					double dist = 0;
					switch (distanceFunction) {
					case EUCLIDEAN:
						dist = xdist*xdist + ydist*ydist + zdist*zdist;
						break;
					case MANHATTAN:
						dist = Math.abs(xdist) + Math.abs(ydist) + Math.abs(zdist);
						break;
					case CHEBYSHEV:
						dist = Math.max(Math.max(Math.abs(xdist), Math.abs(ydist)), Math.abs(zdist));
						break;
					case MIN:
						dist = Math.min(Math.min(Math.abs(xdist), Math.abs(ydist)), Math.abs(zdist));
						break;
					}
					
					if (dist < minDist) {
						minDistX = xdist;
						minDistY = ydist;
						minDistZ = zdist;
						minDist = dist;
					}
				}
			}
		}
		
		return new Point3D(initx + minDistX, inity + minDistY, initz + minDistZ);
	}
	
	private int inc(int num, int n) {
		num += n;
		int ret;
		if (num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}

	@Override
	public String getModuleName() {
		return "Cellularize";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		CHEBYSHEV,
		MIN;
	}
	
	private enum CellDistribution {
		VORONOI,
		SQUARE;
	}
	
	private class Point3D {
		public double x, y, z;
		public Point3D(double x, double y, double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
}
