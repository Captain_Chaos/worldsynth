/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleBlockspaceCombiner extends AbstractModule {
	
	private EnumParameter<Operation> operation = new EnumParameter<Operation>("operation", "Arithmetic operation", null, Operation.class, Operation.OR);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				operation
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		Operation operation = this.operation.getValue();
		
		if (!inputs.containsKey("primary") || !inputs.containsKey("secondary")) {
			//If the main or secondary input is null, there is not enough input and then just return null
			return null;
		}
		MaterialState<?, ?>[][][] primary = ((DatatypeBlockspace) inputs.get("primary")).getBlockspace();
		MaterialState<?, ?>[][][] secondary = ((DatatypeBlockspace) inputs.get("secondary")).getBlockspace();
		
		MaterialState<?, ?>[][][] blockspace = new MaterialState<?, ?>[spw][sph][spl];
		
		MaterialState<?, ?> air = MaterialRegistry.getDefaultAir().getDefaultState();
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					MaterialState<?, ?> i0 = primary[u][v][w];
					MaterialState<?, ?> i1 = secondary[u][v][w];
					
					MaterialState<?, ?> o = air;
					
					switch (operation) {
					case AND:
						if (!MaterialRegistry.isAir(i0) && !MaterialRegistry.isAir(i1)) {
							o = i0;
						}
						break;
					case OR:
						if (!MaterialRegistry.isAir(i0)) {
							o = i0;
						}
						else {
							o = i1;
						}
						break;
					case XOR:
						if (!MaterialRegistry.isAir(i0) && MaterialRegistry.isAir(i1)) {
							o = i0;
						}
						else if (MaterialRegistry.isAir(i0) && !MaterialRegistry.isAir(i1)) {
							o = i1;
						}
						break;
					case REPLACE:
						if (!MaterialRegistry.isAir(i0) && !MaterialRegistry.isAir(i1)) {
							o = i0;
						}
						else {
							o = i1;
						}
						break;
					case REMOVE:
						if (MaterialRegistry.isAir(i1)) {
							o = i0;
						}
						else {
							o = air;
						}
						break;
					}
					
					blockspace[u][v][w] = o;
				}
			}
		}
		
		requestData.setBlockspace(blockspace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(1), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace combiner";
	}
	
	@Override
	public String getModuleMetaTag() {
		return operation.getValue().name();
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Primary"),
				new ModuleInput(new DatatypeBlockspace(), "Secondary")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum Operation {
		AND, OR, XOR, REPLACE, REMOVE;
	}
}
