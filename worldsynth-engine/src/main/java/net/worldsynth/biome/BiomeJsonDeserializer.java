/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class BiomeJsonDeserializer<B extends Biome> extends StdDeserializer<B> {
	private static final long serialVersionUID = 3260748173889490096L;

	protected BiomeJsonDeserializer() {
		this(null);
	}
	
	protected BiomeJsonDeserializer(Class<?> vc) {
		super(vc);
	}
	
	@Override
	public B deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		String idName = (String) ctxt.findInjectableValue("idName", null, null);
		File biomesFile = (File) ctxt.findInjectableValue("biomesFile", null, null);
		return new BiomeBuilder<B>(idName, node, biomesFile).createBiome();
	}
}
