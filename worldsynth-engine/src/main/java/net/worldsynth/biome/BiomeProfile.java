/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class BiomeProfile<B extends Biome> {
	private static final Logger logger = LogManager.getLogger(BiomeProfile.class);
	
	private final String profileName;
	protected final LinkedHashMap<String, B> biomes = new LinkedHashMap<String, B>();
	
	public BiomeProfile(String profileName) {
		this.profileName = profileName;
	}
	
	public final String getProfileName() {
		return profileName;
	}
	
	public final void parseBiomesFromJsonNode(ObjectNode node, File biomesFile) {
		node.fields().forEachRemaining(e -> {
			addBiome(parseBiomeFromJsonNode(e.getKey(), e.getValue(), biomesFile));
		});
	}
	
	protected final B parseBiomeFromJsonNode(String idName, JsonNode node, File biomesFile) {
		ObjectMapper objectMapper = new ObjectMapper();
		InjectableValues.Std injectableValues = new InjectableValues.Std();
		injectableValues.addValue("idName", idName);
		injectableValues.addValue("biomesFile", biomesFile);
		objectMapper.setInjectableValues(injectableValues);

		logger.info("Parsing biome with idName: " + idName);
		return objectMapper.convertValue(node, getBiomeTypeReference());
	}
	
	protected TypeReference<B> getBiomeTypeReference() {
		return new TypeReference<B>() {};
	}
	
	protected final void addBiome(B biome) {
		biomes.put(biome.getIdName(), biome);
	}
	
	public List<B> getBiomes() {
		return new ArrayList<B>(biomes.values());
	}
	
	public B getBiome(String biomeIdName) {
		return biomes.get(biomeIdName);
	}
	
	public B getDefaultBiome() {
		return getBiomes().get((biomes.size() > 1) ? 1 : 0);
	}
	
	@Override
	public String toString() {
		return profileName;
	}
}
