package net.worldsynth.standalone.ui.parameters;

import java.util.ArrayList;
import java.util.List;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.parameter.AbstractParameter;

public abstract class ParameterUiElement<T extends Object> {
	
	protected final AbstractParameter<T> parameter;
	protected T uiValue;
	
	private List<EventHandler<Event>> changeEventHandlers;
	
	public ParameterUiElement(AbstractParameter<T> parameter) {
		this.parameter = parameter;
		uiValue = parameter.getValue();
	}
	
	public void addChengeEventHandler(EventHandler<Event> handler) {
		if (changeEventHandlers == null) {
			changeEventHandlers = new ArrayList<EventHandler<Event>>();
		}
		
		changeEventHandlers.add(handler);
	}
	
	public void removeChangeEventHandler(EventHandler<Event> handler) {
		if (changeEventHandlers == null) return;
		changeEventHandlers.remove(handler);
	}
	
	protected void notifyChangeHandlers() {
		if (changeEventHandlers == null) return;
		for (EventHandler<Event> handler: changeEventHandlers) {
			handler.handle(null);
		}
	}
	
	public void applyUiValue() {
		parameter.setValue(uiValue);
	}
	
	public T getValue() {
		return uiValue;
	}
	
	public abstract void setDisable(boolean disable);
	
	public abstract void addToGrid(GridPane pane, int row);
}
