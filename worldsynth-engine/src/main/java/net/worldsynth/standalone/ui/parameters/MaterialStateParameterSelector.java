/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import net.worldsynth.fx.control.MaterialStateSelector;
import net.worldsynth.material.MaterialState;
import net.worldsynth.parameter.MaterialStateParameter;

public class MaterialStateParameterSelector extends ParameterUiElement<MaterialState<?, ?>> {
	
	private Label nameLabel;
	private MaterialStateSelector materialStateDropdownSelector;
	
	public MaterialStateParameterSelector(MaterialStateParameter parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Dropdown selector
		materialStateDropdownSelector = new MaterialStateSelector(uiValue);
		materialStateDropdownSelector.valueProperty().addListener(new ChangeListener<MaterialState<?, ?>>() {

			@Override
			public void changed(ObservableValue<? extends MaterialState<?, ?>> observable, MaterialState<?, ?> oldValue, MaterialState<?, ?> newValue) {
				uiValue = newValue;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		materialStateDropdownSelector.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(materialStateDropdownSelector, 1, row);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public MaterialStateSelector getDropdown() {
		return materialStateDropdownSelector;
	}
}
