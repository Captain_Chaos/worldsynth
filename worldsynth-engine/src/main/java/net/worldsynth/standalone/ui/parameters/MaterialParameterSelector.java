/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import org.controlsfx.control.PrefixSelectionComboBox;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.parameter.MaterialParameter;

public class MaterialParameterSelector extends ParameterUiElement<Material<?, ?>> {
	
	private Label nameLabel;
	private PrefixSelectionComboBox<Material<?, ?>> materialDropdownSelector;
	
	public MaterialParameterSelector(MaterialParameter parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Dropdown selector
		materialDropdownSelector = new PrefixSelectionComboBox<Material<?, ?>>();
//		materialDropdownSelector.setTypingDelay(1000);
		materialDropdownSelector.getItems().addAll(MaterialRegistry.getMaterialsAlphabetically());
		materialDropdownSelector.getSelectionModel().select(uiValue);
		materialDropdownSelector.valueProperty().addListener(new ChangeListener<Material<?, ?>>() {

			@Override
			public void changed(ObservableValue<? extends Material<?, ?>> observable, Material<?, ?> oldValue, Material<?, ?> newValue) {
				uiValue = newValue;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		materialDropdownSelector.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(materialDropdownSelector, 1, row);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public PrefixSelectionComboBox<Material<?, ?>> getDropdown() {
		return materialDropdownSelector;
	}
}
