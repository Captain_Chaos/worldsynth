/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.standalone.ui.parameters.DoubleParameterField;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class DoubleParameter extends AbstractParameter<Double> {
	
	protected double min;
	protected double max;
	
	protected double sliderMin;
	protected double sliderMax;
	protected double displayMultiplier;
	
	protected UiType uiType;
	
	public DoubleParameter(String name, String displayName, String description, double defaultValue, double min, double max) {
		this(name, displayName, description, defaultValue, min, max, min, max, 1.0f);
	}
	
	public DoubleParameter(String name, String displayName, String description, double defaultValue, double min, double max, double displayMultiplier) {
		this(name, displayName, description, defaultValue, min, max, min, max, displayMultiplier);
	}
	
	public DoubleParameter(String name, String displayName, String description, double defaultValue, double min, double max, double sliderMin, double sliderMax) {
		this(name, displayName, description, defaultValue, min, max, sliderMin, sliderMax, 1.0f);
	}
	
	public DoubleParameter(String name, String displayName, String description, double defaultValue, double min, double max, double sliderMin, double sliderMax, double displayMultiplier) {
		this(name, displayName, description, UiType.SLIDER, defaultValue, min, max, sliderMin, sliderMax, displayMultiplier);
	}
	
	public DoubleParameter(String name, String displayName, String description, UiType uiType, double defaultValue, double min, double max) {
		this(name, displayName, description, uiType, defaultValue, min, max, min, max, 1.0f);
	}
	
	public DoubleParameter(String name, String displayName, String description, UiType uiType, double defaultValue, double min, double max, double displayMultiplier) {
		this(name, displayName, description, uiType, defaultValue, min, max, min, max, displayMultiplier);
	}
	
	public DoubleParameter(String name, String displayName, String description, UiType uiType, double defaultValue, double min, double max, double sliderMin, double sliderMax) {
		this(name, displayName, description, uiType, defaultValue, min, max, sliderMin, sliderMax, 1.0f);
	}
	
	public DoubleParameter(String name, String displayName, String description, UiType uiType, double defaultValue, double min, double max, double sliderMin, double sliderMax, double displayMultiplier) {
		super(name, displayName, description, defaultValue);
		this.min = min;
		this.max = max;
		this.sliderMin = sliderMin;
		this.sliderMax = sliderMax;
		this.displayMultiplier = displayMultiplier;
		this.uiType = uiType;
	}
	
	@Override
	public ParameterUiElement<Double> getUi() {
		if (uiType == UiType.FIELD) {
			return new DoubleParameterField(this);
		}
		return new DoubleParameterSlider(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node) {
		setValue(node.asDouble());
	}
	
	public double getMin() {
		return min;
	}
	
	public double getMax() {
		return max;
	}
	
	public double getSliderMin() {
		return sliderMin;
	}
	
	public double getSliderMax() {
		return sliderMax;
	}
	
	public double getDisplayMultiplier() {
		return displayMultiplier;
	}
	
	public enum UiType {
		SLIDER,
		FIELD;
	}
}
