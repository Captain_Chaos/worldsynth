/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.standalone.ui.parameters.LongParameterField;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class LongParameter extends AbstractParameter<Long> {
	
	protected long min;
	protected long max;
	
	public LongParameter(String name, String displayName, String description, long defaultValue, long min, long max) {
		super(name, displayName, description, defaultValue);
		this.min = min;
		this.max = max;
	}
	
	@Override
	public ParameterUiElement<Long> getUi() {
		return new LongParameterField(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node) {
		setValue(node.asLong());
	}
	
	public long getMin() {
		return min;
	}
	
	public long getMax() {
		return max;
	}
}
