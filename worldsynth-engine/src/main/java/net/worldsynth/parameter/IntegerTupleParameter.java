/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;

public class IntegerTupleParameter extends AbstractNumberTupleParameter<Integer> {

	public IntegerTupleParameter(String name, String displayName, String description, Integer min, Integer max, Integer ... defaultValues) {
		super(name, displayName, description, min, max, defaultValues);
	}

	@Override
	public Integer parseValue(JsonNode valueNode) throws NumberFormatException {
		return valueNode.asInt();
	}
	
	@Override
	public Integer parseValue(String valueString) throws NumberFormatException {
		return Integer.parseInt(valueString);
	}

	@Override
	public boolean insideBounds(Integer value) {
		return  getMin() <= value && value <= getMax();
	}
}
