/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;

public class FloatTupleParameter extends AbstractNumberTupleParameter<Float> {

	public FloatTupleParameter(String name, String displayName, String description, Float min, Float max, Float ... defaultValues) {
		super(name, displayName, description, min, max, defaultValues);
	}

	@Override
	public Float parseValue(JsonNode valueNode) {
		return (float) valueNode.asDouble();
	}
	
	@Override
	public Float parseValue(String valueString) throws NumberFormatException {
		return Float.parseFloat(valueString);
	}

	@Override
	public boolean insideBounds(Float value) {
		return  getMin() <= value && value <= getMax();
	}
}
