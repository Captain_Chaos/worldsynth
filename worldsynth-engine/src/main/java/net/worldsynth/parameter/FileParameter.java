/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import java.io.File;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.standalone.ui.parameters.FileParameterField;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class FileParameter extends AbstractParameter<File> {

	protected boolean save;
	protected boolean directory;
	protected ExtensionFilter fileExtensions;
	
	public FileParameter(String name, String displayName, String description, File defaultValue, boolean save, boolean directory, ExtensionFilter fileExtensions) {
		super(name, displayName, description, defaultValue);
		
		this.save = save;
		this.directory = directory;
		this.fileExtensions = fileExtensions;
	}
	
	@Override
	public ParameterUiElement<File> getUi() {
		return new FileParameterField(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		if (getValue() == null) return objectMapper.convertValue("", JsonNode.class);
		return objectMapper.convertValue(getValue().getPath(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node) {
		setValue(new File(node.asText()));
	}
	
	public boolean isSave() {
		return save;
	}
	
	public boolean isDirectory() {
		return directory;
	}
	
	public ExtensionFilter getFilter() {
		return fileExtensions;
	}
}
