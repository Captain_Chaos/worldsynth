/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.standalone.ui.parameters.table.TableParameterTable;

public abstract class TableParameter<P> extends AbstractParameter<List<P>> {
	
	private List<TableColumn<P, ?>> columns;
	
	@SafeVarargs
	public TableParameter(String name, String displayName, String description, List<P> defaultValue, TableColumn<P, ?> ... columns) {
		super(name, displayName, description, defaultValue);
		this.columns = Arrays.asList(columns);
	}

	@Override
	public ParameterUiElement<List<P>> getUi() {
		return new TableParameterTable<P>(this, columns) {
		};
	}
	
	public abstract P newRow();

	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode arrayNode = objectMapper.createArrayNode();
		
		for (P row: getValue()) {
			arrayNode.add(rowToJson(row));
		}
		return arrayNode;
	}
	
	private JsonNode rowToJson(P row) {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode rowNode = objectMapper.createObjectNode();
		
		for (TableColumn<P, ?> column: columns) {
			rowNode.set(column.getName(), column.toJson(row));
		}
		return rowNode;
	}

	@Override
	public void fromJson(JsonNode node) {
		ArrayList<P> rows = new ArrayList<P>();
		
		for (JsonNode rowNode: node) {
			rows.add(rowFromElement(rowNode));
		}
		setValue(rows);
	}
	
	private P rowFromElement(JsonNode rowNode) {
		P row = newRow();
		for (TableColumn<P, ?> column: columns) {
			column.fromJson(row, rowNode);
		}
		return row;
	}
}
