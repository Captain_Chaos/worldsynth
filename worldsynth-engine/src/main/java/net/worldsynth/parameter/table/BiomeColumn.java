/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.table;

import org.controlsfx.control.PrefixSelectionComboBox;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.scene.control.Control;
import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;

public abstract class BiomeColumn<P> extends TableColumn<P, Biome> {

	public BiomeColumn(String name, String displayName, String description) {
		super(name, displayName, description);
	}

	@Override
	public Control getNewUiControl(Biome value, OnChange notifier) {
		PrefixSelectionComboBox<Biome> biomeDropdownSelector = new PrefixSelectionComboBox<Biome>();
//		materialDropdownSelector.setTypingDelay(1000);
		for (Biome biome: BiomeRegistry.getBiomesAlphabetically()) {
			biomeDropdownSelector.getItems().add(biome);
		}
		biomeDropdownSelector.getSelectionModel().select(value);
		biomeDropdownSelector.valueProperty().addListener((observable, oldValue, newValue) -> {
			notifier.call();
		});
		return biomeDropdownSelector;
	}
	
	@Override
	public Biome getColumnValueFromUiControl(Control control) {
		@SuppressWarnings("unchecked")
		PrefixSelectionComboBox<Biome> biomeDropdownSelector = (PrefixSelectionComboBox<Biome>) control;
		return biomeDropdownSelector.getValue();
	}
	
	@Override
	public JsonNode valueToJson(Biome value) {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(value.getIdName(), JsonNode.class);
	}
	
	@Override
	public Biome valueFromJson(JsonNode node) {
		return BiomeRegistry.getBiome(node.asText());
	}
}
