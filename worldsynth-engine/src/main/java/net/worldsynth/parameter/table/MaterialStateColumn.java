/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.scene.control.Control;
import net.worldsynth.fx.control.MaterialStateSelector;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

public abstract class MaterialStateColumn<P> extends TableColumn<P, MaterialState<?, ?>> {

	public MaterialStateColumn(String name, String displayName, String description) {
		super(name, displayName, description);
	}

	@Override
	public Control getNewUiControl(MaterialState<?, ?> value, OnChange notifier) {
		MaterialStateSelector stateSelector = new MaterialStateSelector(value);
		stateSelector.valueProperty().addListener((observable, oldValue, newValue) -> {
			notifier.call();
		});
		return stateSelector;
	}
	
	@Override
	public MaterialState<?, ?> getColumnValueFromUiControl(Control control) {
		MaterialStateSelector materialStateSelector = (MaterialStateSelector) control;
		return materialStateSelector.getValue();
	}
	
	@Override
	public JsonNode valueToJson(MaterialState<?, ?> value) {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(value.getIdName(), JsonNode.class);
	}
	
	@Override
	public MaterialState<?, ?> valueFromJson(JsonNode node) {
		return MaterialRegistry.getMaterialState(node.asText());
	}
}
