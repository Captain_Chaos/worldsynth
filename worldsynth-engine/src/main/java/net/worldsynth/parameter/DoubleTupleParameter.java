/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;

public class DoubleTupleParameter extends AbstractNumberTupleParameter<Double> {

	public DoubleTupleParameter(String name, String displayName, String description, Double min, Double max, Double ... defaultValues) {
		super(name, displayName, description, min, max, defaultValues);
	}

	@Override
	public Double parseValue(JsonNode valueNode) throws NumberFormatException {
		return valueNode.asDouble();
	}
	
	@Override
	public Double parseValue(String valueString) throws NumberFormatException {
		return Double.parseDouble(valueString);
	}

	@Override
	public boolean insideBounds(Double value) {
		return  getMin() <= value && value <= getMax();
	}
}
