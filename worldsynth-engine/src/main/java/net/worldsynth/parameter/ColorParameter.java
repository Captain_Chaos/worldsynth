/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.worldsynth.color.WsColor;
import net.worldsynth.standalone.ui.parameters.ColorParameterSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class ColorParameter extends AbstractParameter<WsColor> {

	public ColorParameter(String name, String displayName, String description, WsColor defaultValue) {
		super(name, displayName, description, defaultValue);
	}

	@Override
	public ParameterUiElement<WsColor> getUi() {
		return new ColorParameterSelector(this);
	}

	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode colorNode = objectMapper.createObjectNode();
		
		colorNode.put("red", getValue().getRed());
		colorNode.put("green", getValue().getGreen());
		colorNode.put("blue", getValue().getBlue());
		if (getValue().getOpacity() < 1.0f) {
			colorNode.put("opacity", getValue().getOpacity());
		}
		return colorNode;
	}

	@Override
	public void fromJson(JsonNode node) {
		float red = (float) node.get("red").asDouble();
		float green = (float) node.get("green").asDouble();
		float blue = (float) node.get("blue").asDouble();
		float opacity = 1.0f;
		if (node.has("opacity")) {
			opacity = (float) node.get("opacity").asDouble();
		}
		setValue(new WsColor(red, green, blue, opacity));
	}
}
