/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.list;

import java.util.List;
import java.util.function.Supplier;

import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialState;
import net.worldsynth.parameter.table.MaterialStateColumn;
import net.worldsynth.parameter.table.TableColumn;

public class MaterialStateListParameter extends ListParameter<MaterialState<?, ?>> {

	public MaterialStateListParameter(String name, String displayName, String description, List<MaterialState<?, ?>> defaultValue) {
		super(name, displayName, description, defaultValue, new Supplier<TableColumn<Row<MaterialState<?, ?>>, MaterialState<?, ?>>>() {

			@Override
			public TableColumn<Row<MaterialState<?, ?>>, MaterialState<?, ?>> get() {
				return new MaterialStateColumn<Row<MaterialState<?,?>>>("materialstate", "Material state", null) {

					@Override
					public MaterialState<?, ?> getColumnValue(Row<MaterialState<?, ?>> row) {
						return row.rowValue;
					}

					@Override
					public void setColumnValue(Row<MaterialState<?, ?>> row, MaterialState<?, ?> value) {
						row.rowValue = value;
					}
				};
			}
		});
	}

	@Override
	public Row<MaterialState<?, ?>> newRow() {
		return new Row<MaterialState<?, ?>>(Material.NULL.getDefaultState());
	}
}
