/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class EnumParameter<E extends Enum<E>> extends AbstractParameter<E> {
	
	protected Class<E> enumClass;
	protected E[] selectables;
	
	public EnumParameter(String name, String displayName, String description, Class<E> enumClass, E defaultValue) {
		super(name, displayName, description, defaultValue);
		
		this.enumClass = enumClass;
		selectables = enumClass.getEnumConstants();
	}
	
	@SuppressWarnings("unchecked")
	public EnumParameter(String name, String displayName, String description, Class<E> enumClass, E defaultValue, E... selectables) {
		super(name, displayName, description, defaultValue);
		
		this.enumClass = enumClass;
		this.selectables = selectables;
	}
	
	@Override
	public ParameterUiElement<E> getUi() {
		return new EnumParameterDropdownSelector<E>(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue().name(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node) {
		for (E type: selectables) {
			if (node.asText().equals(type.name())) {
				setValue(type);
				return;
			}
		}
	}
	
	public Class<E> getEnumClass() {
		return enumClass;
	}
	
	public E[] getSelectables() {
		return selectables;
	}
}
