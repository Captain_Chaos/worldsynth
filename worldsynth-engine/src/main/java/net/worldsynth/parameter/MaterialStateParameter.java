/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.standalone.ui.parameters.MaterialStateParameterSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class MaterialStateParameter extends AbstractParameter<MaterialState<?, ?>> {
	
	public MaterialStateParameter(String name, String displayName, String description, MaterialState<?, ?> defaultValue) {
		super(name, displayName, description, defaultValue);
	}
	
	@Override
	public ParameterUiElement<MaterialState<?, ?>> getUi() {
		return new MaterialStateParameterSelector(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue().getIdName(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node) {
		setValue(MaterialRegistry.getMaterialState(node.asText()));
	}
}
