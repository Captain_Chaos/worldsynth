/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import java.util.LinkedHashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.standalone.ui.parameters.SelectionSetParameterSelector;

public abstract class AbstractSelctionSetParameter<T> extends AbstractParameter<LinkedHashMap<T, Boolean>> {
	
	public AbstractSelctionSetParameter(String name, String displayName, String description, LinkedHashMap<T, Boolean> defaultSelectionSet) {
		super(name, displayName, description, defaultSelectionSet);
	}
	
	@Override
	public ParameterUiElement<LinkedHashMap<T, Boolean>> getUi() {
		return new SelectionSetParameterSelector<T>(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode objectNode = objectMapper.createObjectNode();
		
		for (T k: getValue().keySet()) {
			objectNode.put(k.toString().toLowerCase(), getValue().get(k));
		}
		return objectNode;
	}

	@Override
	public void fromJson(JsonNode node) {
		for (T k: getValue().keySet()) {
			if (node.has(k.toString().toLowerCase())) {
				getValue().put(k, node.get(k.toString().toLowerCase()).asBoolean());
			}
		}
	}
}
