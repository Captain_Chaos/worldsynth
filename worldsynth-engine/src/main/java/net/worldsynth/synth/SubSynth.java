/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth;

import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.extent.WorldExtentManager;

public class SubSynth extends Synth {
	
	private Synth parrentSynth;
	
	public SubSynth(String name, Synth parrentSynth) {
		super(name);
		this.parrentSynth = parrentSynth;
	}
	
	public SubSynth(JsonNode jsonNode, Synth parrentSynth) {
		super("");
		this.parrentSynth = parrentSynth;
		fromJson(jsonNode, parrentSynth);
	}
	
	public WorldExtentManager getExtentManager() {
		return parrentSynth.getExtentManager();
	}
}
