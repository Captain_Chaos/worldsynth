/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control.skin;

import com.booleanbyte.fx.popupboxbase.control.skin.PopupBoxSkinBase;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import net.worldsynth.fx.control.MaterialStateSelector;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

public class MaterialStateSelectorSkin extends PopupBoxSkinBase<MaterialState<?, ?>> {

	private Label displayNode;
	private DropdownMaterialStateSelector popupContent;

	public MaterialStateSelectorSkin(final MaterialStateSelector materialStateSelector) {
		super(materialStateSelector);
		
		displayNode = new Label();
		displayNode.getStyleClass().add("materialstate-selector-label");
		displayNode.setManaged(false);

		updateMaterialState();

		materialStateSelector.valueProperty().addListener((ob, ov, nv) -> {
			updateMaterialState();
		});
	}

	@Override
	protected double computePrefWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset) {
		String displayNodeText = displayNode.getText();
		double width = 0;
		for (Material<?, ?> material: MaterialRegistry.getMaterialsAlphabetically()) {
			for (MaterialState<?, ?> state: material.getStates()) {
				displayNode.setText(state.getDisplayName());
				width = Math.max(width, super.computePrefWidth(height, topInset, rightInset, bottomInset, leftInset));
			}

		}
		displayNode.setText(displayNodeText);
		return width;
	}

	@Override
	protected Node getPopupContent() {
		if (popupContent == null) {
			popupContent = new DropdownMaterialStateSelector((MaterialStateSelector) getSkinnable());
		}
		return popupContent;
	}

	@Override
	public Node getDisplayNode() {
		return displayNode;
	}

	private void updateMaterialState() {
		final MaterialStateSelector materialStateSelector = (MaterialStateSelector) getSkinnable();
		MaterialState<?, ?> state = materialStateSelector.getValue();
		
		if (state != null) {
			displayNode.setText(state.getDisplayName());
		}
		else {
			displayNode.setText("");
		}
	}

	@Override
	protected StringConverter<MaterialState<?, ?>> getConverter() {
		return null;
	}

	@Override
	protected TextField getEditor() {
		return null;
	}
}
