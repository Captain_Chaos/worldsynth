/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control.skin;

import com.booleanbyte.fx.popupboxbase.control.skin.PopupBoxSkinBase;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import net.worldsynth.fx.control.WsColorPicker;

public class WsColorPickerSkin extends PopupBoxSkinBase<Color> {

	private WsDropdownColorPicker popupContent;

	public WsColorPickerSkin(final WsColorPicker colorPicker) {
		super(colorPicker);

		updateColor();

		colorPicker.valueProperty().addListener((ob, ov, nv) -> {
			updateColor();
		});
	}

	@Override
	protected double computePrefWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset) {
		return 100.0;
	}

	@Override
	protected Node getPopupContent() {
		if (popupContent == null) {
			popupContent = new WsDropdownColorPicker((WsColorPicker) getSkinnable());
		}
		return popupContent;
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public Node getDisplayNode() {
		return null;
	}

	private void updateColor() {
		getSkinnable().setStyle("-fx-color: " + getFormatHexString() + ";" + "-fx-body-color: " + getFormatHexString() + ";");
	}

	private String getFormatHexString() {
		Color c = getSkinnable().getValue();
		if (c != null) {
			return String.format("#%02x%02x%02x", Math.round(c.getRed() * 255), Math.round(c.getGreen() * 255), Math.round(c.getBlue() * 255));
		}
		else {
			return null;
		}
	}

	@Override
	protected javafx.util.StringConverter<Color> getConverter() {
		return null;
	}

	@Override
	protected TextField getEditor() {
		return null;
	}
}
