/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.worldsynth.addon.AddonLoader;
import net.worldsynth.addon.IAddon;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.customobject.CustomObjectFormatRegistry;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.module.NativeModuleRegister;

public class WorldSynth {
	
	public static final Logger LOGGER = LogManager.getLogger(WorldSynth.class);
	
	public static NativeModuleRegister moduleRegister;
	
	public WorldSynth(File worldSynthDirectory) {
		this(worldSynthDirectory, null);
	}
	
	public WorldSynth(WorldSynthDirectoryConfig worldSynthDirectoryConfig) {
		this(worldSynthDirectoryConfig, null);
	}
	
	public WorldSynth(File worldSynthDirectory, IAddon[] injectedAddonLoaders) {
		this(new WorldSynthDirectoryConfig(worldSynthDirectory), injectedAddonLoaders);
	}
	
	public WorldSynth(WorldSynthDirectoryConfig worldSynthDirectoryConfig, IAddon[] injectedAddonLoaders) {
		
		AddonLoader addonLoader = new AddonLoader(worldSynthDirectoryConfig.getAddonDirectory(), injectedAddonLoaders);
		
		//Initialize loaded addons
		LOGGER.info("Initializing loaded addons");
		addonLoader.initAddons(worldSynthDirectoryConfig);
		
		//Initialize the biome registry
		LOGGER.info("Initializing biome registry");
		new BiomeRegistry(worldSynthDirectoryConfig.getBiomesDirectory(), addonLoader);
		//Initialize  the material registry
		LOGGER.info("Initializing material registry");
		new MaterialRegistry(worldSynthDirectoryConfig.getMaterialsDirectory(), addonLoader);
		
		//Initialize the module registry
		LOGGER.info("Initializing module registry");
		moduleRegister = new NativeModuleRegister(addonLoader);
		
		//Initialize the custom object format registry
		LOGGER.info("Initializing custom object format registry");
		new CustomObjectFormatRegistry(addonLoader);
	}
}
