/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.extent;

import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.sun.javafx.event.EventHandlerManager;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;

public class WorldExtentManager {
	private ObservableList<WorldExtent> observableExtentsList = FXCollections.observableArrayList();
	private SimpleObjectProperty<WorldExtent> currentWorldExtent = new SimpleObjectProperty<WorldExtent>();
	
	private final EventHandlerManager eventHandlerManager = new EventHandlerManager(this);
	
	public WorldExtentManager() {
		observableExtentsList = FXCollections.observableArrayList();
		observableExtentsList.add(new WorldExtent("Default extent 1/4k", -128, -128, 256));
		observableExtentsList.add(new WorldExtent("1/2k", -256, -256, 512));
		observableExtentsList.add(new WorldExtent("1k", -512, -512, 1024));
		observableExtentsList.add(new WorldExtent("2k", -1024, -1024, 2048));
		observableExtentsList.add(new WorldExtent("4k", -2048, -2048, 4096));
		
		currentWorldExtent.set(observableExtentsList.get(0));
	}
	
	public WorldExtentManager(JsonNode jsonNode) {
		fromJson(jsonNode);
		currentWorldExtent.set(observableExtentsList.get(0));
	}
	
	public void addWorldExtent(WorldExtent extent) {
		observableExtentsList.add(extent);
		extent.fireExtentAddedEvent();
		eventHandlerManager.dispatchBubblingEvent(new ExtentEvent(ExtentEvent.EXTENT_ADDED, extent));
	}
	
	public void removeWorldExtent(WorldExtent extent) {
		int i = observableExtentsList.indexOf(extent);
		
		if (observableExtentsList.size() == 1) {
			return;
//			WorldExtent defaultExtent = new WorldExtent("Default extent 1/4k", -128, -128, 256);
//			addWorldExtent(defaultExtent);
//			setCurrentWorldExtent(defaultExtent);
		}
		else if (i+1 < observableExtentsList.size()) {
			setCurrentWorldExtent(observableExtentsList.get(i+1));
		}
		else {
			setCurrentWorldExtent(observableExtentsList.get(i-1));
		}
		
		observableExtentsList.remove(extent);
		extent.fireExtentRemovedEvent();
		eventHandlerManager.dispatchBubblingEvent(new ExtentEvent(ExtentEvent.EXTENT_REMOVED, extent));
	}
	
	public ObservableList<WorldExtent> getObservableExtentsList() {
		return observableExtentsList;
	}
	
	public WorldExtent getCurrentWorldExtent() {
		return currentWorldExtent.getValue();
	}
	
	public void setCurrentWorldExtent(WorldExtent extent) {
		currentWorldExtent.setValue(extent);
	}
	
	public SimpleObjectProperty<WorldExtent> currentWorldExtentProperty() {
		return currentWorldExtent;
	}
	
	public WorldExtent getWorldExtentById(long id) {
		for (WorldExtent e: getObservableExtentsList()) {
			if (e.getId() == id) {
				return e;
			}
		}
		return null;
	}
	
	public final <T extends ExtentEvent> void addEventHandler(final EventType<T> eventType, final EventHandler<? super T> eventHandler) {
		getEventHandlerManager().addEventHandler(eventType, eventHandler);
	}

	public final <T extends ExtentEvent> void removeEventHandler(final EventType<T> eventType, final EventHandler<? super T> eventHandler) {
		getEventHandlerManager().removeEventHandler(eventType, eventHandler);
	}

	private EventHandlerManager getEventHandlerManager() {
		return eventHandlerManager;
	}
	
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode extentsNode = objectMapper.createArrayNode();
		
		Iterator<WorldExtent> extentsIterator = observableExtentsList.listIterator();
		while (extentsIterator.hasNext()) {
			extentsNode.add(extentsIterator.next().toJson());
		}
			
		return extentsNode;
	}
	
	protected void fromJson(JsonNode extentsNode) {
		for (JsonNode e: extentsNode) {
			observableExtentsList.add(new WorldExtent(e));
		}
	}
}
