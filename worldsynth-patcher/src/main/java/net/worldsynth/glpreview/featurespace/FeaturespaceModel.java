/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.featurespace;

import net.worldsynth.featurepoint.Featurepoint3D;
import net.worldsynth.glpreview.Point;
import net.worldsynth.glpreview.model.AbstractPointModel;

public class FeaturespaceModel extends AbstractPointModel {
	
	public Featurepoint3D[] points;
	
	private int pointsCount = 0;
	
	public FeaturespaceModel(Featurepoint3D[] points, double x, double y, double z, double width, double height, double length) {
		this.points = points;
		
		generatePoints(points, x, y, z, width, height, length);
	}
	
	@Override
	public int getPrimitivesCount() {
		return pointsCount;
	}
	
	private void generatePoints(Featurepoint3D[] points, double x, double y, double z, double width, double height, double length) {
		pointsCount = points.length;
		initVertexArray(pointsCount);
		
		Point tempPoint = new Point();
		for (int i = 0; i < pointsCount; i++) {
			tempPoint.setVertex((float) (points[i].getX()-x), (float) (points[i].getY()-y), (float) (points[i].getZ()-z));
			tempPoint.setColor(1.0f, 1.0f, 1.0f);
			insertVertexArray(tempPoint, i);
		}
	}
}
