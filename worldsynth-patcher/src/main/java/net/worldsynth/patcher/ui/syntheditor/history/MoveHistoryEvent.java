/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.syntheditor.history;

import net.worldsynth.patch.Patch;
import net.worldsynth.patcher.ui.navcanvas.Coordinate;

public class MoveHistoryEvent extends PatchHistoryEvent {
	
	private Object[] moveSubjects;
	private Coordinate[] oldCoordinates;
	private Coordinate[] newCoordinates;
	
	public MoveHistoryEvent(Patch patch, Object[] moveSubjects, Coordinate[] oldCoordinates, Coordinate[] newCoordinates) {
		super(patch);
		this.moveSubjects = moveSubjects;
		this.oldCoordinates = oldCoordinates;
		this.newCoordinates = newCoordinates;
	}
	
	public Object[] getMoveSubjects() {
		return moveSubjects;
	}
	
	public Coordinate[] getOldCoordinates() {
		return oldCoordinates;
	}
	
	public Coordinate[] getNewCoordinates() {
		return newCoordinates;
	}
	
	public void setOldCoordinates(Coordinate[] oldCoordinates) {
		this.oldCoordinates = oldCoordinates;
	}
	
	public void setNewCoordinates(Coordinate[] newCoordinates) {
		this.newCoordinates = newCoordinates;
	}
}
