/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import com.jogamp.opengl.awt.GLJPanel;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturespace;
import net.worldsynth.glpreview.featurespace.Featurespace3DGLPreviewBufferedGL;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;
import net.worldsynth.synth.SynthParameters;

public class Featurespace3DRender extends AbstractPreviewRender implements GLPreview {
	
	private final Featurespace3DGLPreviewBufferedGL openGL3DRenderer;
	
	public Featurespace3DRender() {
		openGL3DRenderer = new Featurespace3DGLPreviewBufferedGL();
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeFeaturespace castData = (DatatypeFeaturespace) data;
		setFeaturespace(castData, synthParameters.getNormalizedHeightmapHeight());
	}
	
	public void setFeaturespace(DatatypeFeaturespace featurespace, float normalizedHeight) {
		double cx = featurespace.extent.getX();
		double cy = featurespace.extent.getY();
		double cz = featurespace.extent.getZ();
		double cw = featurespace.extent.getWidth();
		double ch = featurespace.extent.getHeight();
		double cl = featurespace.extent.getLength();
		openGL3DRenderer.setFeaturespace(featurespace.getFeaturepoints(), cx, cy, cz, cw, ch, cl, normalizedHeight);
	}

	@Override
	public GLJPanel getGLJpanel() {
		return openGL3DRenderer;
	}
}
