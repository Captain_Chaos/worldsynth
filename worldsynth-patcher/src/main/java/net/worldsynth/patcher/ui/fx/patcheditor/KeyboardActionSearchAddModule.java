/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane.KeyboardListener;

class KeyboardActionSearchAddModule implements KeyboardAction {
	
	private static final KeyboardActionEventFilter FILTER = new KeyboardActionEventFilter(
			KeyEvent.KEY_RELEASED, // Key event type
			KeyCode.SPACE, // Key code
			EventFilterInputState.NO, // Modifier key Ctrl pressed
			EventFilterInputState.NO, // Modifier key Shift pressed
			EventFilterInputState.NO, // Modifier key Alt pressed
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.NO, // Temp connector active
			EventFilterInputState.NO, // Temp patch active
			EventFilterInputState.NO); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, KeyboardListener t) {
		return FILTER.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, KeyboardListener t) {
		editor.setTempWrapper(null);
		if (!editor.moduleSearchPopup.isShowing()) {
			editor.moduleSearchPopup = new ModuleSearchPopupFx(editor);
			editor.moduleSearchPopup.show(editor, editor.mouseListener.currentEvent.getScreenX(), editor.mouseListener.currentEvent.getScreenY());
		}
	}
}
