/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import com.jogamp.opengl.awt.GLJPanel;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.glpreview.voxel.Blockspace3DGLPreviewBufferedGL;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;
import net.worldsynth.synth.SynthParameters;

public class Blockspace3DRender extends AbstractPreviewRender implements GLPreview {
	
	private final Blockspace3DGLPreviewBufferedGL openGL3DRenderer;
	
	public Blockspace3DRender() {
		openGL3DRenderer = new Blockspace3DGLPreviewBufferedGL();
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeBlockspace castData = (DatatypeBlockspace) data;
		setBlockspace(castData, synthParameters.getNormalizedHeightmapHeight());
	}
	
	public void setBlockspace(DatatypeBlockspace blockspace, float normalizedHeight) {
		openGL3DRenderer.setBlockspace(blockspace.getBlockspace(), normalizedHeight);
	}

	@Override
	public GLJPanel getGLJpanel() {
		return openGL3DRenderer;
	}
}
