/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.syntheditor.history;

import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;

public class BypassChangeHistoryEvent extends PatchHistoryEvent {
	
	private ModuleWrapper subject;
	private boolean oldBypassState;
	private boolean newBypassState;
	
	public BypassChangeHistoryEvent(Patch patch, ModuleWrapper subject, boolean oldBypassState, boolean newBypassState) {
		super(patch);
		this.subject = subject;
		this.oldBypassState = oldBypassState;
		this.newBypassState = newBypassState;
	}
	
	public ModuleWrapper getSubject() {
		return subject;
	}
	
	public boolean getOldBypassState() {
		return oldBypassState;
	}
	
	public boolean getNewBypassState() {
		return newBypassState;
	}
}
