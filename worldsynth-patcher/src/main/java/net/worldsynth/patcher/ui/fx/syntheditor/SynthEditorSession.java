/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.control.Tab;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.patch.Patch;
import net.worldsynth.patcher.WorldSynthPatcher;
import net.worldsynth.patcher.data.PatcherDataManager;
import net.worldsynth.patcher.ui.fx.WorldSynthEditor;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane;
import net.worldsynth.patcher.ui.syntheditor.history.EditorHistory;
import net.worldsynth.patcher.ui.syntheditor.history.HistoryEvent;
import net.worldsynth.patcher.ui.syntheditor.history.PatchHistoryEvent;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.SynthManager;

public class SynthEditorSession {
	public static final Logger logger = LogManager.getLogger(SynthEditorSession.class);
	
	private final Synth synth;
	private final WorldSynthEditor editor;
	private final EditorHistory history = new EditorHistory(20);
	
	private final HashMap<Patch, Tab> patchTabs = new HashMap<Patch, Tab>();
	private final HashMap<Patch, PatchEditorPane> patchEditors = new HashMap<Patch, PatchEditorPane>();
	

	private boolean unsavedChanges = false;
	
	public SynthEditorSession(Synth synth, WorldSynthEditor editor) {
		this.synth = synth;
		this.editor = editor;
		
		openPatch(synth.getPatch());
	}
	
	public Synth getSynth() {
		return synth;
	}
	
	public void openPatch(Patch patch) {
		if (patchTabs.containsKey(patch)) {
			editor.editorTabPane.getSelectionModel().select(patchTabs.get(patch));
			return;
		}
		
		// If the patch is not the main patch of the synth, indicate that it is a macro with "[M]"
		Tab patchEditorTab = new Tab(patch == synth.getPatch() ? synth.getName() : "[M] " + synth.getName());
		if (hasUnsavedChanges()) {
			patchEditorTab.setText(patch == synth.getPatch() ? "*" + synth.getName() : "*[M] " + synth.getName());
		}
		
		PatchEditorPane patchEditor = patchEditors.get(patch);
		if (patchEditor == null) {
			patchEditor = new PatchEditorPane(patch, this);
		}
		patchEditorTab.setContent(patchEditor);
		
		patchEditorTab.setOnClosed(e -> {
			patchTabs.remove(patch);
			
			if (patch == synth.getPatch()) {
				editor.closeSynth(synth);
			}
		});
		
		patchTabs.put(patch, patchEditorTab);
		patchEditors.put(patch, patchEditor);
		
		editor.editorTabPane.getTabs().add(patchEditorTab);
		editor.editorTabPane.getSelectionModel().select(patchEditorTab);
	}
	
	public void repaintPatch(Patch patch) {
		if (patchEditors.containsKey(patch)) {
			patchEditors.get(patch).repaint();
		}
	}
	
	public void save() {
		File file = synth.getFile();
		if (file == null) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open WorldSynth project");
			fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));

			file = fileChooser.showSaveDialog(WorldSynthPatcher.primaryStage);
		}
		if (file == null) {
			return;
		}
		
		String synthName = file.getName();
		synthName = synthName.substring(0, synthName.lastIndexOf(".wsynth"));
		renameSynthInEditor(synthName);
		synth.setFile(file);
		
		SynthManager.saveSynth(synth);
		PatcherDataManager.getWorkspaceData().setMostRecentSynth(file);
		
		unsavedChanges = false;
		for (Patch p: patchTabs.keySet()) {
			if (p == synth.getPatch()) {
				patchTabs.get(p).setText(synth.getName());
			}
			else {
				patchTabs.get(p).setText("[M] " + synth.getName());
			}
		}
	}
	
	public void saveAs() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open WorldSynth project");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));

		File file = fileChooser.showSaveDialog(WorldSynthPatcher.primaryStage);
		if (file == null) {
			return;
		}
		synth.setFile(file);
		
		save();
	}
	
	private void renameSynthInEditor(String name) {
		synth.setName(name);
		Tab mainPatchTab = patchTabs.get(synth.getPatch());
		mainPatchTab.setText(name);
	}
	
	public boolean hasUnsavedChanges() {
		return unsavedChanges;
	}
	
	public void registerUnsavedChangePerformed() {
		unsavedChanges = true;
		for (Patch p: patchTabs.keySet()) {
			if (p == synth.getPatch()) {
				patchTabs.get(p).setText("*" + synth.getName());
			}
			else {
				patchTabs.get(p).setText("*[M] " + synth.getName());
			}
		}
		editor.makePreviewRefreshable();
	}
	
	public void closeAllPatches() {
		Iterator<Patch> it = patchTabs.keySet().iterator();
		while (it.hasNext()) {
			Patch p = it.next();
			editor.editorTabPane.getTabs().remove(patchTabs.get(p));
			patchEditors.remove(p);
			it.remove();
		}
	}
	
	public EditorHistory getHistory() {
		return history;
	}
	
	public void addHistoryEvent(HistoryEvent event) {
		history.addHistoryEvent(event);
	}
	
	public void undo() {
		HistoryEvent historyEvent = history.getLastHistoryEvent();
		if (historyEvent != null) {
			if (historyEvent instanceof PatchHistoryEvent) {
				PatchHistoryEvent patchHistoryEvent = (PatchHistoryEvent) historyEvent;
				patchEditors.get(patchHistoryEvent.getPatch()).undo(patchHistoryEvent);
			}
		}
	}
	
	public void redo() {
		HistoryEvent historyEvent = history.getNextHistoryEvent();
		if (historyEvent != null) {
			if (historyEvent instanceof PatchHistoryEvent) {
				PatchHistoryEvent patchHistoryEvent = (PatchHistoryEvent) historyEvent;
				patchEditors.get(patchHistoryEvent.getPatch()).redo(patchHistoryEvent);
			}
		}
	}
}
