/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane.MouseListener;
import net.worldsynth.patcher.ui.navcanvas.Coordinate;
import net.worldsynth.patcher.ui.syntheditor.history.MoveHistoryEvent;

class MouseActionModuleMove implements MouseAction {
	
	private static final MouseActionEventFilter FILTER = new MouseActionEventFilter(
			MouseEvent.MOUSE_DRAGGED, // Mouse event type
			MouseButton.PRIMARY, // Mouse button
			0, // Mouse click count
			EventFilterInputState.YES, // Mouse over module
			EventFilterInputState.NO, // Mouse over module IO
			EventFilterInputState.NO, // Temp module active
			EventFilterInputState.NO, // Temp connector active
			EventFilterInputState.NO, // Temp patch active
			EventFilterInputState.NO); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER.validateFilter(editor, t);
	}

	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		double diffX = t.lastMouseCoordinateX - t.currentMouseCoordinateX;
		double diffY = t.lastMouseCoordinateY - t.currentMouseCoordinateY;
		
		//Create a move event with the old coordinates at first move
		if (t.tempHistoryEventMove == null) {
			//Move multiple selected
			if (editor.selectedWrappers.contains(editor.wrapperOver) && !t.currentEvent.isShortcutDown()) {
				Object[] moveSubjects = new Object[editor.selectedWrappers.size()];
				Coordinate[] oldCoordinates = new Coordinate[editor.selectedWrappers.size()];
				Coordinate[] newCoordinates = new Coordinate[editor.selectedWrappers.size()];
				for (int i = 0; i < editor.selectedWrappers.size(); i++) {
					moveSubjects[i] = editor.selectedWrappers.get(i);
					oldCoordinates[i] = new Coordinate(editor.selectedWrappers.get(i).posX, editor.selectedWrappers.get(i).posY);
					newCoordinates[i] = new Coordinate(editor.selectedWrappers.get(i).posX, editor.selectedWrappers.get(i).posY);
				}
				t.tempHistoryEventMove = new MoveHistoryEvent(editor.patch, moveSubjects, oldCoordinates, newCoordinates);
			}
			//Move single under cursor
			else {
				Object[] moveSubjects = {editor.wrapperOver};
				Coordinate[] oldCoordinates = {new Coordinate(editor.wrapperOver.posX, editor.wrapperOver.posY)};
				Coordinate[] newCoordinates = {new Coordinate(editor.wrapperOver.posX, editor.wrapperOver.posY)};
				t.tempHistoryEventMove = new MoveHistoryEvent(editor.patch, moveSubjects, oldCoordinates, newCoordinates);
			}
		}
		
		Object[] moveSubjects = t.tempHistoryEventMove.getMoveSubjects();
		for (int i = 0; i < moveSubjects.length; i++) {
			//Move device
			ModuleWrapper subject = (ModuleWrapper) moveSubjects[i];
			subject.posX -= diffX;
			subject.posY -= diffY;
			
			//Update latest position for history event
			t.tempHistoryEventMove.getNewCoordinates()[i].x = subject.posX;
			t.tempHistoryEventMove.getNewCoordinates()[i].y = subject.posY;
		}
		
		editor.repaint();
	}

}
