/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.syntheditor.history;

import net.worldsynth.patch.Patch;

public class PatchHistoryEvent extends HistoryEvent {
	
	private final Patch patch;
	
	public PatchHistoryEvent(Patch patch) {
		this.patch = patch;
	}
	
	public Patch getPatch() {
		return patch;
	}
}
