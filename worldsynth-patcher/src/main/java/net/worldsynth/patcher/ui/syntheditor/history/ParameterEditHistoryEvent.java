/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.syntheditor.history;

import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;

public class ParameterEditHistoryEvent extends PatchHistoryEvent {
	
	private ModuleWrapper subject;
	private JsonNode oldParameterJson;
	private JsonNode newParameterJson;
	
	public ParameterEditHistoryEvent(Patch patch, ModuleWrapper subjects, JsonNode oldParameterJson, JsonNode newParameterJson) {
		super(patch);
		this.subject = subjects;
		this.oldParameterJson = oldParameterJson;
		this.newParameterJson = newParameterJson;
	}
	
	public ModuleWrapper getSubject() {
		return subject;
	}
	
	public JsonNode getOldParameterJson() {
		return oldParameterJson;
	}
	
	public JsonNode getNewParameterJson() {
		return newParameterJson;
	}
}
