/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter.Indenter;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane.KeyboardListener;

class KeyboardActionCut implements KeyboardAction {
	private static Logger logger = LogManager.getLogger(KeyboardActionCut.class);
	
	private static final KeyboardActionEventFilter FILTER = new KeyboardActionEventFilter(
			KeyEvent.KEY_RELEASED, // Key event type
			KeyCode.X, // Key code
			EventFilterInputState.YES, // Modifier key Ctrl pressed
			EventFilterInputState.NO, // Modifier key Shift pressed
			EventFilterInputState.NO, // Modifier key Alt pressed
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.UNUSED, // Temp connector active
			EventFilterInputState.UNUSED, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, KeyboardListener t) {
		return FILTER.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, KeyboardListener t) {
		//Find the selected connectors
		ArrayList<ModuleConnector> selectedConnectors = new ArrayList<ModuleConnector>();
		for (ModuleWrapper wrapper: editor.selectedWrappers) {
			for (ModuleConnector connector: editor.patch.getModuleConnectorsByWrapper(wrapper)) {
				if (!selectedConnectors.contains(connector) && editor.selectedWrappers.contains(connector.module2) && editor.selectedWrappers.contains(connector.module1)) {
					selectedConnectors.add(connector);
				}
			}
		}
		
		//Instance a temporary patch
		TempPatch temp = new TempPatch(editor.selectedWrappers, selectedConnectors, editor.patch);
		
		// Serialize the temporary patch
		DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
		Indenter indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
		printer.indentObjectsWith(indenter);
		printer.indentArraysWith(indenter);
		
		String jsonString;
		try {
			jsonString = new ObjectMapper().writer(printer).writeValueAsString(temp.toJson());
		} catch (JsonProcessingException e) {
			logger.error(e);
			return;
		}
		
		// Put the serialized patch on the clipboard
		Clipboard clipboard = Clipboard.getSystemClipboard();
		ClipboardContent content = new ClipboardContent();
		content.putString(jsonString);
		clipboard.setContent(content);
		
		// Remove the cut modules from the patch
		editor.removeModuleWrappers(editor.selectedWrappers.toArray(new ModuleWrapper[0]), true, true);
		editor.clearSelectedWrappers();
	}
}
