/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.syntheditor.history;

import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;

public class NameChangeHistoryEvent extends PatchHistoryEvent {
	
	private ModuleWrapper subject;
	private String oldName;
	private String newName;
	
	public NameChangeHistoryEvent(Patch patch, ModuleWrapper subject, String oldName, String newNmae) {
		super(patch);
		this.subject = subject;
		this.oldName = oldName;
		this.newName = newNmae;
	}
	
	public ModuleWrapper getSubject() {
		return subject;
	}
	
	public String getOldName() {
		return oldName;
	}
	
	public String getNewName() {
		return newName;
	}
}
