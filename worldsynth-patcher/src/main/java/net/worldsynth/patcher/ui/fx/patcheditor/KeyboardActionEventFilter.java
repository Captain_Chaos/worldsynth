/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.event.EventType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane.KeyboardListener;

class KeyboardActionEventFilter {
	private final EventType<KeyEvent> keyEventType;
	private final KeyCode keyCode;
	
	private final EventFilterInputState modifierCtrl;
	private final EventFilterInputState modifierShift;
	private final EventFilterInputState modifierAlt;
	
	private final EventFilterInputState mouseOverModule;
	private final EventFilterInputState mouseOverIo;
	private final EventFilterInputState tempModule;
	private final EventFilterInputState tempConnector;
	private final EventFilterInputState tempPatch;
	
	private final EventFilterInputState areaSelector;
	
	/**
	 * @param keyEventType
	 * @param keyCode
	 * @param modifierCtrl
	 * @param modifierShift
	 * @param modifierAlt
	 * @param mouseOverModule
	 * @param mouseOverIo
	 * @param tempModule
	 * @param tempConnector
	 * @param tempPatch
	 * @param areaSelector
	 */
	public KeyboardActionEventFilter(EventType<KeyEvent> keyEventType, KeyCode keyCode, EventFilterInputState modifierCtrl, EventFilterInputState modifierShift, EventFilterInputState modifierAlt, EventFilterInputState mouseOverModule, EventFilterInputState mouseOverIo, EventFilterInputState tempModule, EventFilterInputState tempConnector, EventFilterInputState tempPatch, EventFilterInputState areaSelector) {
		this.keyEventType = keyEventType;
		this.keyCode = keyCode;
		
		this.modifierCtrl = modifierCtrl;
		this.modifierShift = modifierShift;
		this.modifierAlt = modifierAlt;
		
		this.mouseOverModule = mouseOverModule;
		this.mouseOverIo = mouseOverIo;
		this.tempModule = tempModule;
		this.tempConnector = tempConnector;
		this.tempPatch = tempPatch;
		
		this.areaSelector = areaSelector;
	}
	
	public final boolean validateFilter(PatchEditorPane editor, KeyboardListener t) {
		if (keyEventType != t.currentEvent.getEventType()) {
			return false;
		}
		else if (keyCode != t.currentEvent.getCode()) {
			return false;
		}
		
		else if (modifierCtrl != EventFilterInputState.UNUSED && modifierCtrl.getState() != t.currentEvent.isShortcutDown()) {
			return false;
		}
		else if (modifierShift != EventFilterInputState.UNUSED && modifierShift.getState() != t.currentEvent.isShiftDown()) {
			return false;
		}
		else if (modifierAlt != EventFilterInputState.UNUSED && modifierAlt.getState() != t.currentEvent.isAltDown()) {
			return false;
		}
		
		else if (mouseOverModule != EventFilterInputState.UNUSED && mouseOverModule.getState() != (editor.wrapperOver != null)) {
			return false;
		}
		else if (mouseOverIo != EventFilterInputState.UNUSED && mouseOverIo.getState() != (editor.wrapperIoOver != null)) {
			return false;
		}
		else if (tempModule != EventFilterInputState.UNUSED && tempModule.getState() != (editor.tempWrapper != null)) {
			return false;
		}
		else if (tempConnector != EventFilterInputState.UNUSED && tempConnector.getState() != (editor.tempConnector != null)) {
			return false;
		}
		else if (tempPatch != EventFilterInputState.UNUSED && tempPatch.getState() != (editor.tempPatch != null)) {
			return false;
		}
		else if (areaSelector != EventFilterInputState.UNUSED && areaSelector.getState() != (editor.selectionRectangle != null || editor.selectionLasso != null)) {
			return false;
		}
		
		return true;
	}
}
