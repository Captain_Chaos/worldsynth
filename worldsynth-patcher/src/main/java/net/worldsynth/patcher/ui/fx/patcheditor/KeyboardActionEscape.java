/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane.KeyboardListener;

class KeyboardActionEscape implements KeyboardAction {
	
	private static final KeyboardActionEventFilter FILTER = new KeyboardActionEventFilter(
			KeyEvent.KEY_RELEASED, // Key event type
			KeyCode.ESCAPE, // Key code
			EventFilterInputState.NO, // Modifier key Ctrl pressed
			EventFilterInputState.NO, // Modifier key Shift pressed
			EventFilterInputState.NO, // Modifier key Alt pressed
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.UNUSED, // Temp connector active
			EventFilterInputState.UNUSED, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, KeyboardListener t) {
		return FILTER.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, KeyboardListener t) {
		editor.setTempWrapper(null);
		editor.setTempPatch(null);
		editor.tempConnector = null;
		editor.repaint();
	}
}
