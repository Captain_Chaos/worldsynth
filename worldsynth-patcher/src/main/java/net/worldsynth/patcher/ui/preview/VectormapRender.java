/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class VectormapRender extends AbstractPreviewRenderCanvas {
	
	private float[][][] vectorfield;
	
	private float gain = 10.0f;
	
	double mouseX = 0, mouseY = 0;
	private float[] mouseOverVector = null;
	
	public VectormapRender() {
		setOnScroll(e -> {
			
			float maxGain = 100.0f;
			float minGain = 0.05f;
			
			double lastGain = gain;
			gain += e.getDeltaY()/e.getMultiplierY();
			if (gain < minGain) gain = minGain;
			else if (gain > maxGain) gain = maxGain;
			
			if (lastGain != gain) {
				paint();
			}
		});
		
		setOnMouseMoved(e -> {
			mouseX = e.getX();
			mouseY = e.getY();
			mouseOverVector = vectorAtPixel((int) e.getX(), (int) e.getY());
			paint();
		});
		
		setOnMouseExited(e -> {
			mouseOverVector = null;
			paint();
		});
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeVectormap castData = (DatatypeVectormap) data;
		this.vectorfield = castData.getVectormap();
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setLineWidth(1.0);
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if (vectorfield != null) {
			
			double xOffset = (getWidth() - vectorfield.length)/2;
			double yOffset = (getHeight() - vectorfield[0].length)/2;
			
			for (int x = 0; x < vectorfield.length; x+=10) {
				for (int y = 0; y < vectorfield[x].length; y+=10) {
					double xx = vectorfield[x][y][0];
					double yy = vectorfield[x][y][1];
					
					for (double i = 0.1; i <= 1.0; i+=0.1) {
						g.setLineWidth(i * 1.0);
						g.setStroke(Color.LIGHTGRAY);
						
						double x0 = x+xOffset + xx * gain * (i - 0.1);
						double y0 = y+yOffset + yy * gain * (i - 0.1);
						double x1 = x+xOffset + xx * gain * i;
						double y1 = y+yOffset + yy * gain * i;
						g.strokeLine(x0, y0, x1, y1);
					}
				}
			}
		}
		
		if (mouseOverVector != null) {
			g.setFill(Color.gray(0.4, 0.4));
			g.fillOval(mouseX-gain*10, mouseY-gain*10, gain*20, gain*20);
			g.setStroke(Color.gray(1.0, 0.4));
			g.strokeOval(mouseX-gain, mouseY-gain, gain*2, gain*2);
			g.strokeOval(mouseX-gain*10, mouseY-gain*10, gain*20, gain*20);
			
			double x = mouseX, y = mouseY;
			g.setStroke(Color.CYAN);
			for (int i = 0; i < 2000; i++) {
				float[] v = vectorAtPixel((int) x, (int) y);
				if (v == null) break;
				
				double nx = x + v[0];
				double ny = y + v[1];
				g.strokeLine(x, y, nx, ny);
				x = nx;
				y = ny;
				
			}
			
			g.setLineWidth(2.0);
			g.setStroke(Color.WHITE);
			g.strokeLine(mouseX, mouseY, mouseX + mouseOverVector[0] * gain * 10, mouseY + mouseOverVector[1] * gain * 10);
		}
	}
	
	private float[] vectorAtPixel(int x, int y) {
		int xOffset = ((int) getWidth()-vectorfield.length)/2;
		int yOffset = ((int) getHeight() - vectorfield[0].length)/2;
		
		int mapX = x - xOffset;
		int mapY = y - yOffset;
		
		if (mapX > 0 && mapY > 0 && mapX < vectorfield.length && mapY < vectorfield[0].length) {
			return vectorfield[mapX][mapY];
		}
		
		return null;
	}
}
