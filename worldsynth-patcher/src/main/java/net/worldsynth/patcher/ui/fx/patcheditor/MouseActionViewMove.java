/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane.MouseListener;

class MouseActionViewMove implements MouseAction {
	
	private static final MouseActionEventFilter FILTER_MIDDLE_BUTTON = new MouseActionEventFilter(
			MouseEvent.MOUSE_DRAGGED, // Mouse event type
			MouseButton.MIDDLE, // Mouse button
			0, // Mouse click count
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.UNUSED, // Temp connector active
			EventFilterInputState.UNUSED, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	private static final MouseActionEventFilter FILTER_SECONDARY_BUTTON = new MouseActionEventFilter(
			MouseEvent.MOUSE_DRAGGED, // Mouse event type
			MouseButton.SECONDARY, // Mouse button
			0, // Mouse click count
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.UNUSED, // Temp connector active
			EventFilterInputState.UNUSED, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER_MIDDLE_BUTTON.validateFilter(editor, t) || FILTER_SECONDARY_BUTTON.validateFilter(editor, t);
	}

	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		double diffX = t.lastMouseCoordinateX - t.currentMouseCoordinateX;
		double diffY = t.lastMouseCoordinateY - t.currentMouseCoordinateY;
		
		t.currentMouseCoordinateX += diffX;
		t.currentMouseCoordinateY += diffY;
		
		editor.centerCoordX += diffX;
		editor.centerCoordY += diffY;
		
		editor.repaint();
	}

}
