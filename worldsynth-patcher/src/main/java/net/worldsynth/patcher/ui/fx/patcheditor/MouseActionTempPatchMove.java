/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane.MouseListener;

class MouseActionTempPatchMove implements MouseAction {
	
	private static final MouseActionEventFilter FILTER = new MouseActionEventFilter(
			MouseEvent.MOUSE_MOVED, // Mouse event type
			MouseButton.NONE, // Mouse button
			0, // Mouse click count
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.UNUSED, // Temp connector active
			EventFilterInputState.YES, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		editor.tempPatch.setCenterTo(t.currentMouseCoordinateX, t.currentMouseCoordinateY);
		editor.repaint();
	}
}
