/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import com.jogamp.opengl.awt.GLJPanel;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmapOverlay;
import net.worldsynth.glpreview.heightmap.HeightmapOverlay3DGLPreviewBufferedGL;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;
import net.worldsynth.synth.SynthParameters;

public class HeightmapOverlay3DRender extends AbstractPreviewRender implements GLPreview {
	
	private final HeightmapOverlay3DGLPreviewBufferedGL openGL3DRenderer;
	
	public HeightmapOverlay3DRender() {
		openGL3DRenderer = new HeightmapOverlay3DGLPreviewBufferedGL();
	}

	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeHeightmapOverlay castData = (DatatypeHeightmapOverlay) data;
		setHeightmap(castData, synthParameters.getNormalizedHeightmapHeight());
	}
	
	public void setHeightmap(DatatypeHeightmapOverlay overlay, float normalizedHeight) {
		double size = Math.max(overlay.extent.getWidth(), overlay.extent.getLength());
		openGL3DRenderer.setHeightmap(overlay.heightMap, overlay.colorMap, size, normalizedHeight);
	}

	@Override
	public GLJPanel getGLJpanel() {
		return openGL3DRenderer;
	}
}
