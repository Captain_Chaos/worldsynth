/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher;

import java.io.File;

import net.worldsynth.WorldSynthDirectoryConfig;

public class WorldSynthPatcherDirectoryConfig extends WorldSynthDirectoryConfig {
	
	private File dataDirectory;
	private File templatesDirectory;
	private File examplesDirectory;
	
	public WorldSynthPatcherDirectoryConfig(File worldSynthDirectory) {
		this(worldSynthDirectory, null, null, null, null, null, null);
	}
	
	public WorldSynthPatcherDirectoryConfig(File worldSynthDirectory, File addonDirectory, File materialsDirectory, File biomesDirectory, File dataDirectory, File templatesDirectory, File examplesDirectory) {
		super(worldSynthDirectory, addonDirectory, materialsDirectory, biomesDirectory);
		this.dataDirectory = initDirectory("data", worldSynthDirectory, dataDirectory);
		this.templatesDirectory = initDirectory("templates", worldSynthDirectory, templatesDirectory);
		this.examplesDirectory = initDirectory("examples", worldSynthDirectory, examplesDirectory);
	}
	
	public File getDataDirectory() {
		return dataDirectory;
	}
	
	public File getTemplatesDirectory() {
		return templatesDirectory;
	}
	
	public File getExamplesDirectory() {
		return examplesDirectory;
	}
}
