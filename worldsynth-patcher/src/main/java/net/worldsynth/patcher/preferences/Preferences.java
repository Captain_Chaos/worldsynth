/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.preferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.controlsfx.control.PropertySheet.Item;

import net.worldsynth.patcher.data.PreferenceData;

public abstract class Preferences {
	
	private final String name;
	private final List<Preferences> subPreferences;
	
	private final List<Preference<?>> preferences = new ArrayList<Preference<?>>();
	
	public Preferences(String name) {
		this.name = name;
		this.subPreferences = null;
	}
	
	public Preferences(String name, Preferences... subPreferences) {
		this.name = name;
		this.subPreferences = Arrays.asList(subPreferences);
	}
	
	public final void setPrefData(PreferenceData prefData) {
		for (Preference<?> pref: preferences) {
			pref.setPrefData(prefData);
		}
		
		if (subPreferences != null) {
			for (Preferences prefs: subPreferences) {
				prefs.setPrefData(prefData);
			}
		}
	}
	
	public final String getName() {
		return name;
	}
	
	public final List<Preferences> getSubPreferences() {
		return subPreferences;
	}
	
	public final List<Item> getPreferenceItems() {
		List<Item> preferenceItems = new ArrayList<Item>();
		
		for (Preference<?> pref: preferences) {
			preferenceItems.add(pref.asItem());
		}
		
		return preferenceItems;
	}
	
	protected final void registerPreference(Preference<?> pref) {
		preferences.add(pref);
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
